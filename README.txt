Hello,

To see this WIP graphics engine in action, run the win64_main.exe file in the
build folder in the source, which you can download from the Downloads section
of this repository. An on-screen GUI is currently under development.

Currently supported platforms: Windows 7 and above.

Thanks,
Aman