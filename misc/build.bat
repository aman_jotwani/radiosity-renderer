@echo off

if not defined DevEnvDir call "w:\misc\buildshell.bat"

set INCLUDE=E:\Code\SDL2-2.0.4\include;%INCLUDE%
set INCLUDE=E:\Code\GI\libs\stb;%INCLUDE%
set INCLUDE=E:\Third-party tools\glew-1.13.0-win32\glew-1.13.0\include;%INCLUDE%
set INCLUDE=E:\Code\GI\libs\imgui-1.48\imgui-1.48;%INCLUDE%

set LIB=E:\Third-party tools\glew-1.13.0-win32\glew-1.13.0\lib\Release\x64;%LIB%
set LIB=E:\Code\SDL2-2.0.4\lib\x64;%LIB%
set LIB=E:\Code\GI\libs\imgui-1.48\build;%LIB%

set CompilerFlags=-MTd -nologo -Gm- -GR- -EHa- -EHs -Zo -FC -Z7 -W3 -D_CRT_SECURE_NO_WARNINGS
set ReleaseFlags=-MT -nologo -Gm- -GR- -EHa- -EHs -D_CRT_SECURE_NO_WARNINGS
set IgnoreWarnings=-wd4100 -wd4201 -wd4838 -wd4244
rem I ignore double to float warnings. How badly can that end?

mkdir ..\build
pushd ..\build
cl %CompilerFlags% %IgnoreWarnings% -Fe:TheMatrix.exe ..\code\win64_main.cpp /link imgui.lib opengl32.lib glew32s.lib SDL2.lib SDL2main.lib -NODEFAULTLIB:LIBCMT
popd
