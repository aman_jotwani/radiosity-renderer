// TODO: For error handling: http://bitsquid.blogspot.ae/2012/01/sensible-error-handling-part-1.html
// REFER: The vertex position data in .obj files is continuous, which means
// that the an object's first vertex position's index will be the
// total_number_of_vertices_in_all_objects_before_current_one + 1
// REFER: The first character of every line decides the parsing decision.
#include "obj_loader.h"
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h> // isspace
#include <stdint.h>

struct Face_Format
{
    int Indices[obj_model::sMaxFaceIndices];
    Vertex_Type Type;
    int VertexCount;
};

// Writes to line till reaching \n or eof
// returns true if reached eof, otherwise false
INTERNAL bool
GetLine(char** Line, char** FilePtr, char* EndOfFile)
{
    char* fcontents = *FilePtr;
    char* line = *Line;
    uint32 line_length = 0;

    // trim the indentation
    while(isspace(fcontents[0]))
    {
        fcontents += 1;
        
        if(fcontents == EndOfFile)
        {
            return(true);
        }
    }

    // this handles the case when file does end with a newline
    if(fcontents == EndOfFile)
    {
        return(true);
    }

    while(fcontents[0] != '\n')
    {
        line[line_length++] = *(fcontents++);

        // this handles the case when the file does not end with a newline
        if(fcontents == EndOfFile)
        {
            return(true);
        }
    }

    fcontents += 1; // skipping over the newline character

    *FilePtr = fcontents;

    return(false);
}
///////////////////////////////////////////////////////////
//// SECTION: Material parser and querying
///////////////////////////////////////////////////////////

INTERNAL void
ParseMtlFile(char* MtlFilename, Array<material_spec>& Materials)
{
    printf("Reading %s.\n", MtlFilename);
    File_Handle mtl_file = Win32ReadEntireFile(MtlFilename);
    
    if(mtl_file.Contents)
    {
        char* fcontents = (char*) mtl_file.Contents;
        char* EndOfString = (char*) mtl_file.Contents + mtl_file.ContentSize;
        material_spec m = {0};
        bool done = false;

        char* line = (char*) malloc(512);

        while(!done)
        {
            memset(line, 0, 512);
            bool eof = GetLine((char**) &line, &fcontents, EndOfString);
            
            if(line[0])
            {
               switch(line[0])
                {
                    case 'n':
                    {
                        if(m.Name[0] != '\0')
                        {
                            Materials.Add(m);
                            m = {0};
                        }

                        sscanf(line, "newmtl %29s\n", m.Name);
                    }
                    break;

                    case 'd':
                    {
                        sscanf(line, "d %f\n", &m.Dissolve);
                    }
                    break;

                    case 'K':
                    {
                        if(line[1] == 'a')
                        {
                            sscanf(line, "Ka %f %f %f\n", &m.Ka.x,
                                                          &m.Ka.y,
                                                          &m.Ka.z);
                        }
                        else if(line[1] == 'd')
                        {
                            sscanf(line, "Kd %f %f %f\n", &m.Kd.x,
                                                          &m.Kd.y,
                                                          &m.Kd.z);
                        }
                        else if(line[1] == 's')
                        {
                            sscanf(line, "Ks %f %f %f\n", &m.Ks.x,
                                                          &m.Ks.y,
                                                          &m.Ks.z);
                        }
                    }
                    break;

                    case 'T':
                    {
                        if(line[1] == 'f')
                        {
                            sscanf(line, "Tf %f %f %f\n", &m.Tf.x,
                                                          &m.Tf.y,
                                                          &m.Tf.z);
                        }
                    }
                    break;

                    case 'N':
                    {
                        if(line[1] == 's')
                        {
                            sscanf(line, "Ns %f\n", &m.Shininess);
                        }
                    } 

                    case 'm':
                    {
                        if(line[4] == 'd')
                        {
                            sscanf(line, "map_d %79s", m.DissolveMask);
                        }

                        if(line[5] == 'd')
                        {
                            sscanf(line, "map_Kd %79s", m.DiffuseMap);
                        }
                        else if(line[5] == 'a')
                        {
                            sscanf(line, "map_Ka %79s", m.AmbientMap);
                        }
                        else if(line[5] == 's')
                        {
                            sscanf(line, "map_Ks %79s", m.SpecMap);
                        }
                        else if(line[4] == 'b')
                        {
                            sscanf(line, "map_bump %79s", m.BumpMap);
                        }
                    }
                    break;

                    case 'i':
                    {
                        sscanf(line, "illum %d\n", &m.Illum);
                    }
                    break;
                }

                if(eof)
                {
                    done = true;
                }
            }
            else if(eof)
            {
                done = true;
            }
        }

        free(line);

        if(m.Name != '\0')
        {
            Materials.Add(m);
        }
    }

    Win32FreeFileMemory(mtl_file.Contents);
}

INTERNAL int32
GetMaterialIndex(Array<material_spec> Materials, char* Name)
{
    for(int i = 0;
        i < Materials.CurrCount;
        ++i)
    {
        if(strncmp(Materials[i].Name, Name, obj_model::sFilenameLen) == 0)
        {
            return(i);
        }
    }
    return(-1);
}

///////////////////////////////////////////////////////////
//// ENDSECTION: Material parser and querying
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//// SECTION: Parsing all the different face formats
///////////////////////////////////////////////////////////

INTERNAL uint32
strcount(char* Line, int Char)
{
    uint32 result = 0;
    Line = strchr(Line, Char);
    while(Line)
    {
        result += 1;
        Line += 1;
        Line = strchr(Line, Char);
    }
    return(result);
}

// Line must be a null terminated string
INTERNAL Face_Format
ExtractFormat(char* Line)
{
    Face_Format result = {};
    uint32 counter = 0;
    uint32 slashes = strcount(Line, '/');

    // string iteration
    Line += 2; // to skip the "f "
    char temp[10] = "";
    bool done = false;
    char* tok = strtok(Line, " /\r");

    while(tok)
    {
        result.Indices[counter++] = (uint32) strtol(tok, NULL, 10);
        tok = strtok(NULL, " /\r");
    } 

    uint32 elements = counter;

    if(slashes  == 6 && 
       elements == 9)
    {
        result.Type         = Vertex_Type::pnt;
        result.VertexCount = 3;
    }
    else if(slashes  == 8 &&
            elements == 12)
    {
        result.Type = Vertex_Type::pnt;
        result.VertexCount = 4;
    }
    else if(slashes  == 3 &&
            elements == 6)
    {
        result.Type         = Vertex_Type::pt;
        result.VertexCount = 3;
    }
    else if(slashes  == 4 &&
            elements == 8)
    {
        result.Type         = Vertex_Type::pt;
        result.VertexCount = 4;
    }
    else if(slashes  == 6 &&
            elements == 6)
    {
        result.Type         = Vertex_Type::pn;
        result.VertexCount = 3;
    }
    else if(slashes  == 8 &&
            elements == 8)
    {
        result.Type         = Vertex_Type::pn;
        result.VertexCount = 4;
    }
    else if(slashes == 0 &&
            elements == 4)
    {
       result.Type = Vertex_Type::p;
       result.VertexCount = 4; 
    }

    return(result);
}

INTERNAL Face
FillFaceStruct(Face_Format Fmt, int CurrVertCount)
{
    Face result = {};
    result.Type = Fmt.Type;
    result.VertexCount = Fmt.VertexCount;

    uint32 j = 0; // counter for Fmt.Indices
    // NOTE: pre-decrement because indices are 1-based in obj files

    switch(result.Type)
    {
        case Vertex_Type::pnt:
        {
            for(int i = 0;
                i < result.VertexCount;
                ++i)
            {
                result.PosIndex[i] = --Fmt.Indices[j++];
                result.TexIndex[i] = --Fmt.Indices[j++];
                result.NormalIndex[i] = --Fmt.Indices[j++]; 
            }
        }
        break;

        case Vertex_Type::pt:
        {
            for(int i = 0;
                i < result.VertexCount;
                ++i)
            {
                result.PosIndex[i] = --Fmt.Indices[j++];
                result.TexIndex[i] = --Fmt.Indices[j++];
            }
        }
        break;

        case Vertex_Type::pn:
        {
            for(int i = 0;
                i < result.VertexCount;
                ++i)
            {
                result.PosIndex[i] = --Fmt.Indices[j++];
                result.NormalIndex[i] = --Fmt.Indices[j++]; 
            }
        }
        break;

        case Vertex_Type::p:
        {
            for(int Index = 0;
                Index < result.VertexCount;
                Index++)
            {
                if(Fmt.Indices[j] < 0)
                {
                    result.PosIndex[Index] = CurrVertCount + Fmt.Indices[j++];
                }
            }
        }
        break;
    }

    return(result);
}

///////////////////////////////////////////////////////////
//// ENDSECTION: Parsing all the different face formats
///////////////////////////////////////////////////////////

obj_model ParseObjFile(int8* memory, char* filename)
{
    File_Handle obj_file = Win32ReadEntireFile(filename);
    obj_model model = {};

    if(obj_file.Contents && memory)
    {
        model.Materials.Allocate((material_spec*) memory, obj_model::sMaterialMemoryBytes);
        memory += obj_model::sMaterialMemoryBytes;

        model.Positions.Allocate((v3*) memory, obj_model::sBufferMemoryBytes);
        memory += obj_model::sBufferMemoryBytes;

        model.TexCoords.Allocate((v3*) memory, obj_model::sBufferMemoryBytes);
        memory += obj_model::sBufferMemoryBytes;

        model.Normals.Allocate((v3*) memory, obj_model::sBufferMemoryBytes);
        memory += obj_model::sBufferMemoryBytes;

        model.Faces.Allocate((Face*) memory, obj_model::sPolyMemoryBytes);
        // memory += sPolyMemoryBytes;
                               
        char* fcontents = (char*) obj_file.Contents;
        char* EndOfString = (char*) obj_file.Contents + obj_file.ContentSize;
        
        int32 curr_material = -1;
        v3 temp = {};
        bool done = false;
        char* line = (char*) malloc(512);

        while(!done)
        {
            memset(line, 0, 512);
            bool eof = GetLine((char**) &line, &fcontents, EndOfString);
            
            if(line[0])
            {
                switch(line[0])
                {
                    case 'm':
                    {
                        char mtl_fname[obj_model::sFilenameLen] = "";
                        sscanf(line, "mtllib %s\n", mtl_fname);
                        ParseMtlFile(mtl_fname, model.Materials);
                    }
                    break;

                    case 'v':
                    {
                        temp = {0};

                        if(line[1] == 'n')
                        {   
                            int success = sscanf(line, "vn %f %f %f\n", &temp.x,
                                                                        &temp.y,
                                                                        &temp.z);
                            model.Normals.Add(temp);
                        }
                        else if(line[1] == 't')
                        {
                            int success = sscanf(line, "vt %f %f %f\n", &temp.x,
                                                                        &temp.y,
                                                                        &temp.z);
                            if(success != 3)
                            {
                                success = sscanf(line, "vt %f %f\n", &temp.x,
                                                                     &temp.y);
                            }
                            
                            model.TexCoords.Add(temp);
                        }
                        else
                        {
                            int success = sscanf(line, "v %f %f %f\n", &temp.x,
                                                                       &temp.y,
                                                                       &temp.z);
                            model.Positions.Add(temp);
                        }
                    }
                    break;

                    // REFER: OBJ Indices start with 1.
                    // Also, only %d/%d/%d case is handled.
                    // other formats like %d/%d or %d//%d are not handled.
                    case 'f':
                    {
                        // NOTE: ExtractFormat uses strtok,
                        // which modifies the string sent to it.
                        Face_Format fmt = ExtractFormat(line);
                        Face f = FillFaceStruct(fmt, model.Positions.CurrCount);
                        f.MatIndex = (uint32) curr_material;
                        model.Faces.Add(f);
                    }
                    break;

                    case 'u':
                    {
                        // REFER: if the vertex Type changes, set curr_material = -1
                        char mat_name[obj_model::sFilenameLen];
                        sscanf(line, "usemtl %29s", mat_name);
                        int32 index = GetMaterialIndex(model.Materials, mat_name);
                        if(index != -1)
                        {
                            curr_material = index;
                        }
                    }
                    break;
                }

                if(eof)
                {
                    done = true;
                }
            }
            else if(eof)
            {
                done = true;
            }
        }

        free(line);
        Win32FreeFileMemory(obj_file.Contents);    
        model.IsValid = true;
        return(model);
    }
    else
    {
        model.IsValid = false;
        return(model);
    }
}
