// TODO: add filename string handling
// unix wants /, windows wants \\

#define global_variable static
#define Assert(Exp) if(!(Exp)) {*(int*)0 = 0;}
#define ArrayCount(Array) sizeof((Array)) / sizeof((Array)[0])
#define Kilobytes(n) (n)*1024
#define Megabytes(n) (n)*1024*1024

#include <stdint.h>
typedef uint32_t u32; 
typedef uint64_t u64;
typedef float r32;
typedef float real32; 
typedef double real64; 
typedef int32_t bool32;

#include <imgui.h>
#include "sdl.h"
#include "platform_defs.h"
#include "mathlib.h"

struct Mem_Block
{
	uint32_t total_size; // in bytes
	int8_t* base;
	uint32_t used; // in bytes
};

void*
PushData(size_t kBytes, Mem_Block* Memory)
{
	Assert(Memory->used + kBytes <= Memory->total_size);
	void* Result = Memory->base + Memory->used;
	Memory->used += kBytes;
	
	return(Result);
}

#define PushStruct(Type, Memory) (Type*)PushData(sizeof(Type), Memory)
#define AllocateArray(Type, Elems, Memory) (Type*)PushData(sizeof(Type)*(Elems), Memory)

#include "render_engine.h"
#include "render_engine.cpp"

#include "ui.cpp"

int main(void)
{
	u32 screen_width = 800;
	u32 screen_height = 760;
	u32 gl_major = 3;
	u32 gl_minor = 3;
	bool running = true;

	Platform_Data sdl;
	if(InitSDL(&sdl, "Graphics Engine",
			   screen_width, screen_height,
			   gl_major, gl_minor))
	{
		Mem_Block game_memory;
		game_memory.total_size = Megabytes(2);
		game_memory.base = (int8_t*) malloc(game_memory.total_size);
		game_memory.used = 0;

		render_engine Renderer;
		
		CreateShader(&(Renderer.DefaultShader),
			"../shaders/light_vshader.glsl", 
			"../shaders/light_fshader.glsl");

		InitTexture(&(Renderer.DefaultTexture),
			"../assets/box.jpg",
			false);

		Renderer.Proj = PerspProjectionLH(0.1f, 100.0f, 45.0f, 
			(float) screen_width / (float) screen_height);

		GLuint Vao = CreateMesh(gVertices, gIndices, sizeof(gVertices), sizeof(gIndices));

		mesh Cube;
		LoadOBJ(&game_memory, Cube, "../textured_cube.obj");

		Camera eye;
		eye.Target = ZeroVector();
		eye.Position = {0.0f, 4.0f, -4.0f};
		eye.WorldUp = {0.0f, 1.0f, 0.0f};
		eye.Speed = 3.0f;
		eye.Pitch = 0.0f;
		eye.Yaw = 90.0f;
		eye.CalcAxes();

		real32 rot_angle = 360.0f;
		
		Material mat;
		mat.Kd = {1.0f, 1.0f, 1.0f};
		mat.Ka = {1.0f, 1.0f, 1.0f};

		Light point_light;
		point_light.Position = {-2.0f, 4.0f, -5.0f};
		point_light.Ambient = {0.2f, 0.2f, 0.2f};
		point_light.Diffuse = {1.0f, 1.0f, 1.0f};

		SetupUiVars(screen_width, screen_height);

		while(running)
		{
			SDL_Event event;
			while(SDL_PollEvent(&event) != 0)
			{
				if(event.type == SDL_KEYDOWN &&
					event.key.keysym.sym == SDLK_ESCAPE)
				{
					running = false;
				}
				// else if(event.type == SDL_KEYDOWN || event.type == SDL_KEYUP)
				// {
					// ImGuiIO& io = ImGui::GetIO();
					// int key = event.key.keysym.sym & ~SDLK_SCANCODE_MASK;
					// io.KeysDown[key] = (event.type == SDL_KEYDOWN);
					// io.KeyShift = ((SDL_GetModState() & KMOD_SHIFT) != 0);
					// io.KeyCtrl = ((SDL_GetModState() & KMOD_CTRL) != 0);
					// io.KeyAlt = ((SDL_GetModState() & KMOD_ALT) != 0);
				// }
			}

			eye.CalcLookAt();
			NewUiFrame(16.67f);
            static float vec3[3] = { 0.10f, 0.20f, 0.30f};
            ImGui::InputFloat3("input float3", vec3);
            // SDL_Log("%.2f %.2f %.2f", vec3[0], vec3[1], vec3[2]);
			// bool opened = true;
			// ImGui::ShowTestWindow(&opened);

			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glViewport(0, 0, screen_width, screen_height);
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);

			glUseProgram(Renderer.DefaultShader.Program);
			
			glBindTexture(GL_TEXTURE_2D, Renderer.DefaultTexture.Handle);
			
			glBindVertexArray(Vao);
			
			// matrix Model = IdentityMatrix();
			matrix Model = Mul(Translation(1.0f, 0.0f, 0.0f), RotationY(rot_angle));
			// Model = Mul(Translation(.0f, 0.0f, 0.0f), Model); // BUG: why is it translating it in the opposite direction
			rot_angle -= 1.f;

			BindMatrix(Renderer.DefaultShader.Program, 
				"model", Model);
			BindMatrix(Renderer.DefaultShader.Program,
				"view", eye.LookAt);
			BindMatrix(Renderer.DefaultShader.Program,
				"proj", Renderer.Proj);
			BindMatrix(Renderer.DefaultShader.Program,
				"uv_transform", IdentityMatrix());
			
			BindVector(Renderer.DefaultShader.Program,
				"light.Position", point_light.Position);
			BindVector(Renderer.DefaultShader.Program,
				"light.Ambient", point_light.Ambient);
			BindVector(Renderer.DefaultShader.Program,
				"light.Diffuse", point_light.Diffuse);
			BindVector(Renderer.DefaultShader.Program,
				"material.Kd", mat.Kd);
			BindVector(Renderer.DefaultShader.Program,
				"material.Ka", mat.Ka);

			SimpleDraw(ArrayCount(gIndices));

			Model = Mul(Translation(-1.0f, 0.0f, 0.0f), RotationY(rot_angle));
			BindMatrix(Renderer.DefaultShader.Program,
				"model", Model);

			DrawMesh(Cube);

			ImGui::Render();

			SDL_GL_SwapWindow(sdl.Window);
		}
	}
	return(0);
}