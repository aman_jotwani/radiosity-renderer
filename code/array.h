#pragma once
#include <stdint.h>
#include <stdlib.h> // for memset, malloc, free

#ifndef DEBUG_ASSERT
#define DEBUG_ASSERT(Exp) if(!(Exp)) {*(int*)0 = 0;}
#endif

template<typename T>
struct Array
{
	int32_t CurrCount;
	int32_t TotalCount;
	T* First;

	T& operator[](int Index);

	Array<T>();
	void Add(T Value);
	void HeapAllocate(int NumBytes);
	void HeapAllocateItems(int NumItems);
	void HeapFree();
	void Allocate(T* Base, int NumBytes);
	void AllocateItems(T* Base, int NumItems);
	void ClearToZero();
};

template<typename T>
Array<T>::Array()
{
	First = NULL;
	CurrCount = 0;
	TotalCount = 0;
}

template<typename T>
void Array<T>::ClearToZero()
{
	memset(First, 0, sizeof(T)*CurrCount);
	CurrCount = 0;
}

template<typename T>
void Array<T>::HeapAllocate(int NumBytes)
{
	First = (T*) malloc(NumBytes);
	TotalCount = NumBytes / sizeof(T);
	CurrCount = 0;
}

template<typename T>
void Array<T>::HeapAllocateItems(int NumItems)
{
	First = (T*) malloc((int)sizeof(T) * NumItems);
	TotalCount = NumItems;
	memset(First, 0, sizeof(T)*TotalCount);
	CurrCount = 0;
}

template<typename T>
void Array<T>::HeapFree()
{
	free(First);
	TotalCount = 0;
	CurrCount = 0;
}

template<typename T>
T& Array<T>::operator[](int Index)
{
	return((First[Index]));
}

template<typename T> 
void Array<T>::Add(T value)
{
	DEBUG_ASSERT(CurrCount < TotalCount)
	First[CurrCount++] = value;
}

template<typename T>
void Array<T>::Allocate(T* Base, int NumBytes)
{
	First = Base;
	TotalCount = NumBytes / sizeof(T);
	CurrCount = 0;
}

template<typename T>
void Array<T>::AllocateItems(T* Base, int NumItems)
{
	First = Base;
	TotalCount = NumItems;
	CurrCount = 0;
}