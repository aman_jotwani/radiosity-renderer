class Unit_Cube
{
	GLuint Vao;
	uint32 NumIndices;

public:
	Unit_Cube();
	void Render();
};

v3 vert0 = {0.0f, 0.0f, 0.0f};
v3 vn0 = {-1.0f, -1.0f, -1.0f};

v3 vert1 = {0.0f, 1.0f, 0.0f};
v3 vn1 = {-1.0f, 1.0f, -1.0f};

v3 vert2 = {1.0f, 1.0f, 0.0f};
v3 vn2 = {1.0f, 1.0f, -1.0f};

v3 vert3 = {1.0f, 0.0f, 0.0f};
v3 vn3 = {1.0f, -1.0f, -1.0f};

v3 vert4 = {0.0f, 0.0f, 1.0f};
v3 vn4 = {-1.0f, -1.0f, 1.0f};

v3 vert5 = {0.0f, 1.0f, 1.0f};
v3 vn5 = {-1.0f, 1.0f, 1.0f};

v3 vert6 = {1.0f, 1.0f, 1.0f};
v3 vn6 = {1.0f, 1.0f, 1.0f};

v3 vert7 = {1.0f, 0.0f, 1.0f};
v3 vn7 = {1.0f, -1.0f, 1.0f};

v3 verts[] = {vert0, vert1, vert2, vert3, vert4, vert5, vert6, vert7};
v3 vns[] = {vn0, vn1, vn2, vn3, vn4, vn5, vn6, vn7};

#define splat(E) (E).x, (E).y, (E).z
#define DEBUG_add_face(A, B, C, D)  splat(verts[A]), 0.0f, 1.0f, 0.0f, splat(vns[A]), \
									splat(verts[B]), 0.0f, 0.0f, 0.0f, splat(vns[B]), \
									splat(verts[C]), 1.0f, 0.0f, 0.0f, splat(vns[C]), \
									splat(verts[D]), 1.0f, 1.0f, 0.0f, splat(vns[D]),


GLuint
CreateCubeMesh(GLfloat* Vertices, GLuint* Indices, size_t VertexBytes, size_t IndexBytes)
{
    GLuint vao, vbo, ebo;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ebo);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, VertexBytes, Vertices, GL_STATIC_DRAW);
    
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*) 0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*) (3 * sizeof(GLfloat)));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*) (6 * sizeof(GLfloat)));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, IndexBytes, Indices, GL_STATIC_DRAW);

    glBindVertexArray(0);

    return(vao);
}


Unit_Cube::Unit_Cube()
{

	GLfloat Vertices[] = 
	{
		DEBUG_add_face(0, 1, 2, 3)
		DEBUG_add_face(3, 2, 6, 7)
		DEBUG_add_face(7, 6, 5, 4)
		DEBUG_add_face(4, 5, 1, 0)
		DEBUG_add_face(1, 5, 6, 2)
		DEBUG_add_face(0, 3, 7, 4)
	};

	GLuint Indices[] = 
	{
		0, 1, 2,
		2, 3, 0,

		4, 5, 6,
		6, 7, 4,

		8, 9, 10,
		10, 11, 8,

		12, 13, 14,
		14, 15, 12,

		16, 17, 18,
		18, 19, 16,

		20, 21, 22,
		22, 23, 20
	};

	Vao = CreateCubeMesh(Vertices, Indices, sizeof(Vertices), sizeof(Indices));
	NumIndices = ArrayCount(Indices);
}

void
Unit_Cube::Render()
{
	glBindVertexArray(Vao);
	SimpleDraw(NumIndices);
}