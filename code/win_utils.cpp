#include "platform_defs.h"
#include <windows.h>

static uint32_t 
SafeTruncateUInt64(uint64_t Value)
{
	Assert(Value <= 0xFFFFFFFF)
	return((uint32_t)Value);
}

int64_t
InitPerfFreq()
{
	LARGE_INTEGER Temp;
	QueryPerformanceFrequency(&Temp);
	return(Temp.QuadPart);
    
    // REFER: Debug only code, rdtsc is inaccurate if the thread it is called from is not bound 
    // to a single processor core
    // Assert(SetThreadAffinityMask(GetCurrentThread(), 1) != 0) // will only run on the first core
}

int64_t 
GetTimeCounter()
{
	LARGE_INTEGER Value;
	QueryPerformanceCounter(&Value);
	return(Value.QuadPart);
}

double 
GetSecondsElapsedSince(int64_t Start, int64_t Freq)
{
	LARGE_INTEGER End;
	QueryPerformanceCounter(&End);
	double Time = (double)(End.QuadPart - Start);
	Time /= (double)Freq;
	return(Time);
}

File_Handle
ReadEntireFile(const char* Filename)
{
    File_Handle Result = {0};
    HANDLE FileHandle = CreateFileA(Filename,
                                    GENERIC_READ,
                                    FILE_SHARE_READ,
                                    0,
                                    OPEN_EXISTING,
                                    0,
                                    0);
    if(FileHandle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER FileSize;
        if(GetFileSizeEx(FileHandle, &FileSize))
        {
            uint32_t FileSize32 = SafeTruncateUInt64(FileSize.QuadPart);
            Result.Contents = VirtualAlloc(0, FileSize32, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
            if(Result.Contents)
            {
                DWORD BytesRead = 0;
                if(ReadFile(FileHandle, Result.Contents, FileSize32, &BytesRead, 0) && 
                  (BytesRead == FileSize32))
                {
                    Result.ContentSize = FileSize32;
                }
                else
                {
                    FreeFileMemory(Result.Contents);
                    Result.Contents = 0;
                } 
            }
            else
            {

            }
        }
        else
        {

        }
        CloseHandle(FileHandle);        
    }
    else
    {

    }
    return(Result);
}

void
FreeFileMemory(void* Memory)
{
    if(Memory)
    {
        VirtualFree(Memory, 0, MEM_RELEASE);
    }
}