#include <imgui.h>

struct ui_system
{
	gl_handle mFontTexture; 
	gl_handle mVao;
	gl_handle mVbo;
	gl_handle mEbo;
	gl_handle mShaderProgram;
	bool      mMouseHeld[2];

	void Init(int ScreenWidth, int ScreenHeight, const char* VshaderFilename, const char* FshaderFilename);
	void Render();
	void HandleEvents(SDL_Event Event);
	void NewFrame(SDL_Window* Window, real64 DeltaTime);
};

void
ui_system::Render()
{
	ImGui::Render();
	ImDrawData* DrawData = ImGui::GetDrawData();

	// Backup GL state
	GLint last_program; glGetIntegerv(GL_CURRENT_PROGRAM, &last_program);
	GLint last_texture; glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
	GLint last_array_buffer; glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
	GLint last_element_array_buffer; glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &last_element_array_buffer);
	GLint last_vertex_array; glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);
	GLint last_blend_src; glGetIntegerv(GL_BLEND_SRC, &last_blend_src);
	GLint last_blend_dst; glGetIntegerv(GL_BLEND_DST, &last_blend_dst);
	GLint last_blend_equation_rgb; glGetIntegerv(GL_BLEND_EQUATION_RGB, &last_blend_equation_rgb);
	GLint last_blend_equation_alpha; glGetIntegerv(GL_BLEND_EQUATION_ALPHA, &last_blend_equation_alpha);
    GLint last_viewport[4]; glGetIntegerv(GL_VIEWPORT, last_viewport);
	GLboolean last_enable_blend = glIsEnabled(GL_BLEND);
	GLboolean last_enable_cull_face = glIsEnabled(GL_CULL_FACE);
	GLboolean last_enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
	GLboolean last_enable_scissor_test = glIsEnabled(GL_SCISSOR_TEST);

	ImGuiIO& io = ImGui::GetIO();
	glViewport(0, 0, (GLsizei) io.DisplaySize.x, (GLsizei) io.DisplaySize.y);
	
	matrix OrthoProj;
	OrthoProj.ColumnMajor[0] = 2.0f / io.DisplaySize.x;
	OrthoProj.ColumnMajor[5] = 2.0f / -io.DisplaySize.y;
	OrthoProj.ColumnMajor[10] = -1.0f;
	OrthoProj.ColumnMajor[12] = -1.0f;
	OrthoProj.ColumnMajor[13] = 1.0f;
	OrthoProj.ColumnMajor[15] = 1.0f;

	// change your shader
	glUseProgram(mShaderProgram);
	glUniform1i(glGetUniformLocation(mShaderProgram, "Texture"), 0);
	glUniformMatrix4fv(glGetUniformLocation(mShaderProgram, "Proj"), 1, GL_FALSE, (const GLfloat*) OrthoProj.ColumnMajor);
	glBindVertexArray(mVao);

	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_SCISSOR_TEST);
	glActiveTexture(GL_TEXTURE0);

	// iterate through command lists
	for(int n = 0; n < DrawData->CmdListsCount; ++n)
	{
		const ImDrawList* cmd_list = DrawData->CmdLists[n];
		const ImDrawIdx* idx_buffer_offset = 0;

		glBindBuffer(GL_ARRAY_BUFFER, mVbo);
		glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr) cmd_list->VtxBuffer.size() * sizeof(ImDrawVert), (GLvoid*) &(cmd_list->VtxBuffer.front()), GL_STREAM_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEbo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr) cmd_list->IdxBuffer.size() * sizeof(ImDrawIdx), (GLvoid*) &(cmd_list->IdxBuffer.front()), GL_STREAM_DRAW);

		for(const ImDrawCmd* pcmd = cmd_list->CmdBuffer.begin(); pcmd != cmd_list->CmdBuffer.end(); pcmd++)
		{
			if (pcmd->UserCallback)
			{
				pcmd->UserCallback(cmd_list, pcmd);
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
				glScissor((int)pcmd->ClipRect.x, (int)(io.DisplaySize.y - pcmd->ClipRect.w), (int)(pcmd->ClipRect.z - pcmd->ClipRect.x), (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
				glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, idx_buffer_offset);
			}
			idx_buffer_offset += pcmd->ElemCount;

		}
	}

	// Restore GL state
	glUseProgram(last_program);
	glBindTexture(GL_TEXTURE_2D, last_texture);
	glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, last_element_array_buffer);
	glBindVertexArray(last_vertex_array);
	glBlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha);
	glBlendFunc(last_blend_src, last_blend_dst);
	if (last_enable_blend) glEnable(GL_BLEND); else glDisable(GL_BLEND);
	if (last_enable_cull_face) glEnable(GL_CULL_FACE); else glDisable(GL_CULL_FACE);
	if (last_enable_depth_test) glEnable(GL_DEPTH_TEST); else glDisable(GL_DEPTH_TEST);
	if (last_enable_scissor_test) glEnable(GL_SCISSOR_TEST); else glDisable(GL_SCISSOR_TEST);
    glViewport(last_viewport[0], last_viewport[1], (GLsizei)last_viewport[2], (GLsizei)last_viewport[3]);
}

void
ui_system::HandleEvents(SDL_Event event)
{
	mMouseHeld[0] = mMouseHeld[1] = false;

	if(event.type == SDL_MOUSEBUTTONDOWN)
	{
		if(event.button.button == SDL_BUTTON_LEFT)
			mMouseHeld[0] = true;
		else if(event.button.button == SDL_BUTTON_RIGHT)
			mMouseHeld[1] = true;
	}
}

void
ui_system::Init(int ScreenWidth, int ScreenHeight,
				const char* VshaderFilename, const char* FshaderFilename)
{
	ImGuiIO& io = ImGui::GetIO();

	io.DisplaySize.x = (float)ScreenWidth;
	io.DisplaySize.y = (float)ScreenHeight;
	// io.RenderDrawListsFn = NULL;

	// the font atlas texture
	unsigned char* pixels;
	int width, height;
	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);
	glGenTextures(1, &mFontTexture);
	glBindTexture(GL_TEXTURE_2D, mFontTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

	io.Fonts->TexID = (void*)(intptr_t)mFontTexture;
	glBindTexture(GL_TEXTURE_2D, 0);

	mShaderProgram = CompileShaders(VshaderFilename, FshaderFilename);

	glGenVertexArrays(1, &mVao);
	glBindVertexArray(mVao);
	glGenBuffers(1, &mVbo);
	glGenBuffers(1, &mEbo);

	glBindBuffer(GL_ARRAY_BUFFER, mVbo);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, pos));
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, uv));
		glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, col));
	#undef OFFSETOF

	glBindVertexArray(0);
}


void
ui_system::NewFrame(SDL_Window* Window, real64 DeltaTime)
{
	ImGuiIO& io = ImGui::GetIO();
	io.DeltaTime = DeltaTime;
	io.MouseDrawCursor = true; // making this false breaks things for some reason, UPDATE: not anymore!

	int mx, my;
	Uint32 mouseMask = SDL_GetMouseState(&mx, &my);
	if (SDL_GetWindowFlags(Window) & SDL_WINDOW_MOUSE_FOCUS)
		io.MousePos = ImVec2((float)mx, (float)my);   // Mouse position, in pixels (set to -1,-1 if no mouse / on another screen, etc.
	else
		io.MousePos = ImVec2(-1, -1);

	io.MouseDown[0] = mMouseHeld[0] || ((mouseMask & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0);		// If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
	io.MouseDown[1] = mMouseHeld[1] || ((mouseMask & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0);

	ImGui::NewFrame();
}

