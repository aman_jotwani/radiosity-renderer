#include "sdl.h"
#include <string.h>


bool 
IsPointInRect(const SDL_Point& Point, const SDL_Rect& Rect)
{
    if(Point.x >= Rect.x && Point.x <= Rect.x + Rect.w &&
       Point.y >= Rect.y && Point.y <= Rect.y + Rect.h)
    {
        return(true);
    }
    else
    {
        return(false);
    }
}

bool IsSameColor(SDL_Color First, SDL_Color Second)
{
    if(First.r == Second.r &&
       First.g == Second.g &&
       First.b == Second.b)
    {
        return(true);
    }
    else
    {
        return(false);
    }
}