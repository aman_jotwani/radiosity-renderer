#include <string.h>

struct memory_block
{
    uint64 mTotalBytes; 
    uint8_t* mBase;
    uint64 mUsedBytes; 
};

void*
PushData(memory_block* Memory, uint64 NumBytes)
{
    Assert(Memory->mUsedBytes + NumBytes <= Memory->mTotalBytes);
    void* Result = Memory->mBase + Memory->mUsedBytes;
    Memory->mUsedBytes += NumBytes;
    
    return(Result);
}

void
PopData(memory_block* Memory, uint64 NumBytes)
{
    Memory->mUsedBytes -= NumBytes;
    memset(Memory->mBase + Memory->mUsedBytes, 0, NumBytes);
}

memory_block
CarveBlock(memory_block* Arena, uint64 NumBytes)
{
    memory_block result;
    result.mBase = (uint8_t*) PushData(Arena, NumBytes);
    result.mTotalBytes = NumBytes;
    result.mUsedBytes = 0;
    return(result);
}

#define PushStruct(Type, Memory) (Type*)PushData((Memory), sizeof(Type))
#define AllocateArray(Type, Elems, Memory) (Type*)PushData((Memory), sizeof(Type)*(Elems))
