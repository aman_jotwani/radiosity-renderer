#pragma warning(push, 0)
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#pragma warning(pop)

void SimpleDraw(uint32 NumIndices);
void BindTexture(GLuint TexUnit, GLuint TexHandle, GLuint Program, char* UniformName);
#define BindMatrix(Shader, Name, Mat) \
    glUniformMatrix4fv(glGetUniformLocation((Shader).Program, (Name)), 1, GL_FALSE, (GLfloat*) ((Mat).ColumnMajor));
#define BindVector(Shader, Name, Vector) \
    glUniform3fv(glGetUniformLocation((Shader).Program, (Name)), 1, (const GLfloat*) &(Vector));

enum class Vertex_Spec
{
	Ptn,
	Pt,
	Pn
};

struct Vertex
{
	v3 Position;
	v3 TexCoord;
	v3 Normal;
	vec4 Tangent;
};

struct Shader_Program
{
	GLuint Program;
	GLuint VertShader;
	GLuint FragShader;
};

struct Texture
{
	GLuint Handle;
	int Width;
	int Height;
};

struct Material
{
	Texture AmbientMap;
	Texture DiffuseMap;
	Texture SpecularMap;
	Texture BumpMap;
	Texture DissolveMask;
	v3 kd;
	v3 ka;
	v3 ks;
	v3 tf;
	float shininess;
	float dissolve;
	uint32 illum;
};

class Camera
{
	public:
		v3 Target;
		v3 Position;
		v3 WorldUp;
		real32 Speed;

		v3 Up;
		v3 Right;
		v3 Front;

		real32 Pitch;
		real32 Yaw;
		matrix LookAt;

		void CalcLookAt();
		void CalcAxes();
};

class Cubemap
{
    GLuint Vao;
    Texture CubemapTexture;
    Shader_Program Shaders;

    public:
        void Create(const char* right, const char* left,
                    const char* up, const char* down,
                    const char* back, const char* front);
        
        void Render(matrix Projection, matrix View);
};

struct Mesh
{
	GLuint Vao;
	Vertex_Spec Spec;
	uint32 IndexCount;
	uint32 MatIndex;
	uint32 ShaderIndex;
};

struct Light
{
	v3 Position;
	v3 Ambient;
	v3 Diffuse;
	v3 Specular;
	real32 LinearAtt;
	real32 QuadAtt;
};

struct render_engine
{
	matrix mProjectionMatrix;
	Array<Shader_Program> mShaderCache;
	Array<Material> mMaterialCache;
	Array<Mesh> mMeshes;
};
