
void
SimpleDraw(uint32 NumIndices)
{
    glDrawElements(GL_TRIANGLES, NumIndices, GL_UNSIGNED_INT, 0);
}

void
BindTexture(GLuint TexUnit, GLuint TexHandle, GLuint ProgramHandle, char* UniformName)
{
    glActiveTexture(GL_TEXTURE0 + TexUnit);
    glBindTexture(GL_TEXTURE_2D, TexHandle);
    glUniform1i(glGetUniformLocation(ProgramHandle, UniformName), TexUnit);
}

///////////////////////////////////////////////////////////
//// SECTION: Camera
///////////////////////////////////////////////////////////

void
Camera::CalcAxes()
{
    Front = Target - Position;
    Front.Normalize();
    WorldUp.Normalize();

    Right = CrossProduct(Front, WorldUp);
    Right.Normalize();

    Up = CrossProduct(Right, Front);
    Up.Normalize();
}

void
Camera::CalcLookAt()
{
    matrix T = Transpose(CreateFromColumns(Right, Up, Front, ZeroVector()));
    LookAt = Mul(T, Translation(-Position.x, -Position.y, -Position.z));
}

///////////////////////////////////////////////////////////
//// ENDSECTION: Camera
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//// SECTION: Shader loading
///////////////////////////////////////////////////////////

void
ReadShaderSource(const char* Filename, GLuint Shader)
{
    File_Handle shader_src = Win32ReadEntireFile(Filename);              // provided by platform
    glShaderSource(Shader, 1, (const GLchar* const*)&shader_src.Contents, NULL);
    glCompileShader(Shader);
    
    GLint success;
    glGetShaderiv(Shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        GLchar info_log[512];
        glGetShaderInfoLog(Shader, 512, NULL, info_log);
        SDL_Log((char*)info_log);
    }

    Win32FreeFileMemory(shader_src.Contents);
}

GLuint
CompileShaders(const char* VShaderFilename, const char* FShaderFilename)
{
    GLuint VertShader = glCreateShader(GL_VERTEX_SHADER);
    ReadShaderSource(VShaderFilename, VertShader);

    GLuint FragShader = glCreateShader(GL_FRAGMENT_SHADER);
    ReadShaderSource(FShaderFilename, FragShader);

    GLuint Program = glCreateProgram();
    glAttachShader(Program, VertShader);
    glAttachShader(Program, FragShader);
    glLinkProgram(Program);

    return(Program);
}

void 
CreateShader(Shader_Program* Shaders, const char* VShaderFilename, const char* FShaderFilename)
{
    Shaders->VertShader = glCreateShader(GL_VERTEX_SHADER);
    ReadShaderSource(VShaderFilename, Shaders->VertShader);

    Shaders->FragShader = glCreateShader(GL_FRAGMENT_SHADER);
    ReadShaderSource(FShaderFilename, Shaders->FragShader);

    Shaders->Program = glCreateProgram();
    glAttachShader(Shaders->Program, Shaders->VertShader);
    glAttachShader(Shaders->Program, Shaders->FragShader);
    glLinkProgram(Shaders->Program);
    // error checking
}

///////////////////////////////////////////////////////////
//// ENDSECTION: Shader loading
///////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//// SECTION: Texture loading
///////////////////////////////////////////////////////////

void
ApplyWhiteColorKey(uint8_t* Data, int Width, int Height)
{
    int kComponents = 4;
    int kTotalBytes = Width*Height*4;

    uint32_t* temp = (uint32_t*)Data;
    for(int i = 0; i < kTotalBytes / 4; ++i)
    {
        if((temp[i] << 8) == 0xFFFFFF00)
        {
            temp[i] = 0;
        }
    }
}

void
InitTexture(Texture* Tex, const char* Filename, bool32 ApplyColorKey)
{
    GLuint MagFilter = GL_LINEAR;
    GLuint MinFilter = GL_LINEAR_MIPMAP_LINEAR;
    GLuint WrapFlag = GL_REPEAT;

    int width, height, comp;
    unsigned char* data = stbi_load(Filename, &width, &height, &comp, 4);
    if(data)
    {
        glGenTextures(1, &Tex->Handle);

        if(ApplyColorKey)
        {
            ApplyWhiteColorKey((uint8_t*) data, width, height);
        }

        glBindTexture(GL_TEXTURE_2D, (Tex->Handle));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, WrapFlag);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, WrapFlag);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, MinFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, MagFilter);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, 0);    
    }
    else
    {
        SDL_Log("While loading %s, %s", Filename, stbi_failure_reason());
    }
    Tex->Width = width;
    Tex->Height = height;
    stbi_image_free(data);
}

///////////////////////////////////////////////////////////
//// ENDSECTION: Texture loading
///////////////////////////////////////////////////////////

void
Cubemap::Create(const char* right, const char* left,
                const char* up, const char* down,
                const char* back, const char* front)
{
    GLfloat vertices[] =
    {
        -1.0f,  1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

        -1.0f,  1.0f, -1.0f,
         1.0f,  1.0f, -1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f,  1.0f
    };
    
    glGenVertexArrays(1, &Vao);
    glBindVertexArray(Vao);
    GLuint vbo;

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), (const GLvoid*) vertices,
                 GL_STATIC_DRAW);
    
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    
    glBindVertexArray(0);

    CreateShader(&Shaders,
                 "../code/shaders/cubemap_vshader.glsl",
                 "../code/shaders/cubemap_fshader.glsl");

    glGenTextures(1, &CubemapTexture.Handle);
    glBindTexture(GL_TEXTURE_CUBE_MAP, CubemapTexture.Handle);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    
    int w, h, n;
    unsigned char* mapright = stbi_load(right, &w, &h, &n, 4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X,
                 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, mapright);
    unsigned char* mapleft = stbi_load(left, &w, &h, &n, 4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 
                 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, mapleft);
    unsigned char* mapup = stbi_load(up, &w, &h, &n, 4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
                 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, mapup);
    unsigned char* mapdown = stbi_load(down, &w, &h, &n, 4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
                 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, mapdown);
    unsigned char* mapback = stbi_load(back, &w, &h, &n, 4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
                 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, mapback);
    unsigned char* mapfront = stbi_load(front, &w, &h, &n, 4);
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
                 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, mapfront);
    
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0); 
}

void
Cubemap::Render(matrix projection, matrix view)
{
	glDepthMask(GL_FALSE); 
    // glDepthFunc(GL_LEQUAL);
	glBindVertexArray(Vao);
	glUseProgram(Shaders.Program);
	BindMatrix(Shaders, "projection", projection);
	BindMatrix(Shaders, "view", StripTranslation(view));

	glBindTexture(GL_TEXTURE_CUBE_MAP, CubemapTexture.Handle);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
    // glDepthFunc(GL_LESS);
    glDepthMask(GL_TRUE);
}

///////////////////////////////////////////////////////////
//// SECTION: Mesh loading and rendering
///////////////////////////////////////////////////////////

Mesh
SetupPtnMesh(Array<Vertex> VertexBuffer, Array<v3i> IndexBuffer)
{
    Mesh result;
    glGenVertexArrays(1, &result.Vao);

    GLuint Vbo, Ebo;
    glGenBuffers(1, &Vbo);
    glGenBuffers(1, &Ebo);

    glBindVertexArray(result.Vao);
    glBindBuffer(GL_ARRAY_BUFFER, Vbo);
    glBufferData(GL_ARRAY_BUFFER, VertexBuffer.CurrCount*sizeof(Vertex), 
                 (const GLvoid*) VertexBuffer.First, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 13 * sizeof(GLfloat), (GLvoid*)0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 13 * sizeof(GLfloat), (GLvoid*) (3 * sizeof(GLfloat)));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 13 * sizeof(GLfloat), (GLvoid*) (6 * sizeof(GLfloat)));

    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 13 * sizeof(GLfloat), (GLvoid*) (9 * sizeof(GLfloat)));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, IndexBuffer.CurrCount*sizeof(v3i),
                 (const GLvoid*) IndexBuffer.First, GL_STATIC_DRAW);

    glBindVertexArray(0);

    return(result);
}

struct Triangle
{
    v3 Positions[3];
    v3 TexCoords[3];
};

void CalcTangentBitangent(vec4* tangent, v3* bitangent, Triangle triangle)
{
    v3 p0 = triangle.Positions[0], p1 = triangle.Positions[1], p2 = triangle.Positions[2];
    v3 q1 = p1 - p0;
    v3 q2 = p2 - p0;
    real32 u0 = triangle.TexCoords[0].x;
    real32 v0 = triangle.TexCoords[0].y;
    real32 u1 = triangle.TexCoords[1].x;
    real32 v1 = triangle.TexCoords[1].y;
    real32 u2 = triangle.TexCoords[2].x;
    real32 v2 = triangle.TexCoords[2].y;

    real32 s1 = u1 - u0;
    real32 s2 = u2 - u0;
    real32 t1 = v1 - v0;
    real32 t2 = v2 - v0;
    real32 denom = 1.0f / (s1*t2 - s2*t1);

    v3 tang = (q1*t2 - q2*t1)*denom;

    *tangent = {tang.x, tang.y, tang.z, 1.0f};
    *bitangent = (q2*s1 - q1*s2)*denom;
}

// this will call ParseObjFile and then use the parse data to fill the Model struct
// think about what all you need to do when you render multiple models, what does the 
// renderer need for a set of models instead of what it needs for a single model
void
LoadOBJ(char* ObjFilename, memory_block* TransientMemory, render_engine* Renderer)
{
    obj_model model = ParseObjFile((int8*) PushData(TransientMemory, obj_model::sMemReqd),
                                   ObjFilename);
    // assuming model.IsValid = true
    uint32 mat_cache_offset = Renderer->mMaterialCache.CurrCount;

    Material stub_material = {};
    for(int i = 0;
        i < model.Materials.CurrCount;
        ++i)
    {
        material_spec* it = &model.Materials[i];
        InitTexture(&stub_material.DiffuseMap,
                    (char*) it->DiffuseMap,
                    false);
        InitTexture(&stub_material.AmbientMap,
                    (char*) it->AmbientMap,
                    false);
        InitTexture(&stub_material.BumpMap,
                    (char*) it->BumpMap,
                    false);
        InitTexture(&stub_material.SpecularMap,
                    (char*) it->SpecMap,
                    false);
        InitTexture(&stub_material.DissolveMask,
                    (char*) it->DissolveMask,
                    false);
        stub_material.kd = it->Kd;
        stub_material.ka = it->Ka;
        stub_material.ks = it->Ks;
        stub_material.shininess = it->Shininess * 4; // because of blinn phong?
        stub_material.tf = it->Tf;
        stub_material.illum = it->Illum;

        Renderer->mMaterialCache.Add(stub_material);
        stub_material = {};
    }

    Array<Vertex> ptn_vbo;
    Array<v3i> ebo;

    ptn_vbo.Allocate((Vertex*) PushData(TransientMemory, Megabytes(50)),
                     Megabytes(50));
    ebo.Allocate((v3i*) PushData(TransientMemory, Megabytes(50)),
                 Megabytes(50));

    // calculate smooth normals
    Array<vec4> smooth_normals;
    smooth_normals.Allocate((vec4*) PushData(TransientMemory, obj_model::sBufferMemoryBytes),
                            obj_model::sBufferMemoryBytes);
    for(int i = 0;
        i < model.Faces.CurrCount;
        ++i)
    {
        Face f = model.Faces[i];
        for(int j = 0;
            j < f.VertexCount;
            ++j)
        {
            v3 n = model.Normals[f.NormalIndex[j]];
            vec4 sn = {n.x, n.y, n.z, 1.0f};
            smooth_normals[f.PosIndex[j]] += sn;
        }
    }

    // calculate tangents and bitangents
    Array<vec4> tangents;
    Array<v3> bitangents;
    arrayAlloc(tangents, TransientMemory, obj_model::sBufferMemoryBytes);
    arrayAlloc(bitangents, TransientMemory, obj_model::sBufferMemoryBytes);

    for(int i = 0;
        i < model.Faces.CurrCount;
        ++i)
    {
        Face f = model.Faces[i];
        vec4 t;
        v3 bt;
        Triangle tri;
        for(int j = 0; j < 3; ++j)
        {
            tri.Positions[j] = model.Positions[f.PosIndex[j]];
            tri.TexCoords[j] = model.TexCoords[f.TexIndex[j]];
        }

        CalcTangentBitangent(&t, &bt, tri);
        for(int j = 0; j < 3; ++j)
        {
            tangents[f.PosIndex[j]] += t;
            bitangents[f.PosIndex[j]] += bt;
        }

        if(f.VertexCount == 4)
        {
            tri.Positions[0] = model.Positions[f.PosIndex[2]];
            tri.TexCoords[0] = model.TexCoords[f.TexIndex[2]];
            tri.Positions[1] = model.Positions[f.PosIndex[3]];
            tri.TexCoords[1] = model.TexCoords[f.TexIndex[3]];
            tri.Positions[2] = model.Positions[f.PosIndex[0]];
            tri.TexCoords[2] = model.TexCoords[f.TexIndex[0]];

            CalcTangentBitangent(&t, &bt, tri);

            tangents[f.PosIndex[2]] += t;
            tangents[f.PosIndex[3]] += t;
            tangents[f.PosIndex[0]] += t;
            bitangents[f.PosIndex[2]] += bt;
            bitangents[f.PosIndex[3]] += bt;
            bitangents[f.PosIndex[0]] += bt;
        }
    }

    // orthonormalize tangents and store handedness in them
    // NOTE: smooth normals, tangents and bitangents are 1-to-1 mapped to every vertex position
    for(int i = 0;
        i < model.Positions.CurrCount;
        ++i)
    {
        v3 t = {tangents[i].x, tangents[i].y, tangents[i].z};
        v3 n = {smooth_normals[i].x, smooth_normals[i].y, smooth_normals[i].z};

        // cgs = classical gram schmidt
        // assuming angle between our required orthonormal tangent and computed tangent is small
        v3 cgs_result = (t - n * DotProduct(n, t));
        real32 handedness = (DotProduct(CrossProduct(n, t), bitangents[i]) < 0.0f) ? -1.0f : 1.0f;
        tangents[i] = {cgs_result.x, cgs_result.y, cgs_result.z, handedness};
    }

    uint32 vcount = 0;
    uint32 curr_material = model.Faces[0].MatIndex;
    Vertex_Type curr_vtype = model.Faces[0].Type;

    for(int i = 0;
        i < model.Faces.CurrCount;
        ++i)
    {
        Face cface = model.Faces[i];

        if(cface.MatIndex != curr_material)
        {
            Mesh m = SetupPtnMesh(ptn_vbo, ebo);
            m.IndexCount = 3*ebo.CurrCount;
            m.MatIndex = curr_material + mat_cache_offset;
            m.ShaderIndex = 0; // default

            Renderer->mMeshes.Add(m);

            vcount = 0;
            curr_material = cface.MatIndex;
            ptn_vbo.ClearToZero();
            ebo.ClearToZero();
        }


        for(int j = 0;
            j < cface.VertexCount;
            ++j)
        {
            vec4 sn = smooth_normals[cface.PosIndex[j]];
            v3 surf_normal = {sn.x, sn.y, sn.z};
            Vertex v = {model.Positions[cface.PosIndex[j]],
                            model.TexCoords[cface.TexIndex[j]],
                            surf_normal,
                            tangents[cface.PosIndex[j]]};
            ptn_vbo.Add(v);
        }

        if(cface.VertexCount == 3)
        {
            v3i index_set1 = {vcount, vcount + 1, vcount + 2};
            ebo.Add(index_set1);
        }
        else if(cface.VertexCount == 4)
        {
            v3i index_set1 = {vcount, vcount + 1, vcount + 2};
            v3i index_set2 = {vcount + 2, vcount + 3, vcount};

            ebo.Add(index_set1);
            ebo.Add(index_set2);
        }

        vcount += cface.VertexCount;
    }

    Mesh m = SetupPtnMesh(ptn_vbo, ebo);
    m.IndexCount = 3*ebo.CurrCount;
    m.MatIndex = curr_material + mat_cache_offset;
    m.ShaderIndex = 0; // default

    Renderer->mMeshes.Add(m);

    PopData(TransientMemory, obj_model::sBufferMemoryBytes);
    PopData(TransientMemory, Megabytes(100));
    PopData(TransientMemory, obj_model::sMemReqd);
}
///////////////////////////////////////////////////////////
//// ENDSECTION: Mesh loading and rendering
///////////////////////////////////////////////////////////

void
Render(render_engine* Renderer, matrix LookAt, Light PointLight, v3 CameraPosition, ui_vars UiVars)
{
    matrix vertical_flip_uvs = IdentityMatrix();
    vertical_flip_uvs.ColumnMajor[5] = -1.0f;
    vertical_flip_uvs.ColumnMajor[13] = 1.0f;

    for(int i = 0;
        i < Renderer->mMeshes.CurrCount;
        ++i)
    {
        Mesh it = Renderer->mMeshes[i];
        Shader_Program shader = Renderer->mShaderCache[it.ShaderIndex]; 
        
        glUseProgram(shader.Program);

        BindMatrix(shader, "proj", Renderer->mProjectionMatrix);
        BindMatrix(shader, "uvTransform", vertical_flip_uvs);
        BindMatrix(shader, "view", LookAt);
        BindVector(shader, "CameraPosition", CameraPosition);
        glUniform1f(glGetUniformLocation(shader.Program, "scaleFactor"), UiVars.mBumpScaleFactor);

        Material mat = Renderer->mMaterialCache[it.MatIndex];
        
        BindVector(shader, "light.Position", PointLight.Position);
        BindVector(shader, "light.Ambient", PointLight.Ambient);
        BindVector(shader, "light.Diffuse", PointLight.Diffuse);
        BindVector(shader, "light.Specular", PointLight.Specular);
        BindVector(shader, "material.Kd", mat.kd);
        BindVector(shader, "material.Ka", mat.ka);
        BindVector(shader, "material.Ks", mat.ks);

        // bind floats, shininess, LinearAtt, QuadAtt
        glUniform1f(glGetUniformLocation(shader.Program, "material.Shininess"), mat.shininess);
        glUniform1f(glGetUniformLocation(shader.Program, "light.LinearAttCoeff"), PointLight.LinearAtt);
        glUniform1f(glGetUniformLocation(shader.Program, "light.QuadraticAttCoeff"), PointLight.QuadAtt);

        BindTexture(0, mat.AmbientMap.Handle, shader.Program, "material.AmbientMap");
        BindTexture(1, mat.DiffuseMap.Handle, shader.Program, "material.DiffuseMap");
        BindTexture(2, mat.BumpMap.Handle, shader.Program, "material.BumpMap");
        BindTexture(3, mat.SpecularMap.Handle, shader.Program, "material.SpecMap");
        BindTexture(4, mat.DissolveMask.Handle, shader.Program, "material.DissolveMask");

        glUniform1i(glGetUniformLocation(shader.Program, "material.HasBumpMap"), mat.BumpMap.Handle);
        glUniform1i(glGetUniformLocation(shader.Program, "material.HasDiffuseMap"), 0);
        glUniform1i(glGetUniformLocation(shader.Program, "material.HasAmbientMap"), 0);
        glUniform1i(glGetUniformLocation(shader.Program, "material.HasSpecMap"), 0);
        glUniform1i(glGetUniformLocation(shader.Program, "material.HasDissolveMask"), mat.DissolveMask.Handle);

        glUniform1i(glGetUniformLocation(shader.Program, "IsAttenuationEnabled"), UiVars.mIsAttenuationEnabled);

        glBindVertexArray(it.Vao);
        glDrawElements(GL_TRIANGLES, it.IndexCount, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);
    }
}
