// REFER:
// 1) I use column major matrices in code and in shaders.
// 2) I use left-handed coordinate system throughout the pipeline.

// REFER: Ignored double to float truncation warnings. 

// TODO:
// 1) Read other people's radiosity implementations and hack them. done.
// 2) Implement normal mapping. done.
// 3) Use imgui for implementing debug/logging UI. done. logging remains to be done.
// 4) Order your draw calls for proper blending.
// 5) define SHADER_DIRECTORY, DATA_DIRECTORY, ASSET_DIRECTORY for filepaths
// 6) Multithreading
// 7) Managing shaders for different vertex types
// 8) Get cornell box rendered right.

#define GLOBAL_VARIABLE static
#define Assert(Exp) if(!(Exp)) {*(int*)0 = 0;}
#define ArrayCount(Array) sizeof(Array) / sizeof(Array[0])
#define Kilobytes(Value) ((Value)*1024LL)
#define Megabytes(Value) (Kilobytes(Value)*1024LL)
#define Gigabytes(Value) (Megabytes(Value)*1024LL)
#define Terabytes(Value) (Gigabytes(Value)*1024LL)

#include <stdint.h>
typedef uint8_t uint8;
typedef int32_t int32;
typedef int64_t int64;
typedef uint32_t uint32;
typedef uint64_t uint64;
typedef float real32; 
typedef double real64; 
typedef int32 bool32;

#include "mem_mgmt.cpp"

#define GLEW_STATIC
#include <GL/glew.h>
typedef GLuint gl_handle;

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#elif (__APPLE__ && __MACH__)
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

// to override sdl's main
#undef main 

void
InitSDL(SDL_Window** window, char* title, 
        uint32_t width, uint32_t height,
        uint32_t glMajorVersion, uint32_t glMinorVersion)
{
    SDL_GLContext context;

    if(SDL_Init(SDL_INIT_EVERYTHING) == 0)
    {
        *window = SDL_CreateWindow(title, 
                                   SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                   width, height,
                                   SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL /*| SDL_WINDOW_FULLSCREEN */);
        if(*window)
        {
            SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, glMajorVersion );
            SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, glMinorVersion );
            // SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

            context = SDL_GL_CreateContext( *window );

            if(context)
            {
                glewExperimental = GL_TRUE;
                GLenum glewError = glewInit();
                if(glewError == GLEW_OK)
                {
                    // 1 - 60 fps
                    // 2 - 30 fps
                    SDL_GL_SetSwapInterval(1);

                    SDL_SetRelativeMouseMode(SDL_TRUE);
                    
                    // int AudioFlags = MIX_INIT_FLAC | MIX_INIT_OGG;
                    // if((Mix_Init(AudioFlags) & AudioFlags) != AudioFlags)
                    // {
                    //     SDL_Log("SDL mixer failed to init. system - %s\n", Mix_GetError());
                    //     return(0);
                    // }
                }
                else
                {
                    SDL_Log("glew didnt init.\n");
                }
            }
            else
            {
                SDL_Log("GLContext creation failed.\n");
            }
        }
        else
        {
            SDL_Log("window creation failed.\n");
        }        
    }
    else
    {
        SDL_Log("SDL init failed.\n");
    }
}

#define VC_EXTRALEAN
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

struct File_Handle
{
    uint32_t ContentSize;
    void*    Contents;
};

static uint32_t 
SafeTruncateUInt64(uint64_t Value)
{
    Assert(Value <= 0xFFFFFFFF)
    return((uint32_t)Value);
}

int64_t
Win32InitPerfFreq()
{
    LARGE_INTEGER Temp;
    QueryPerformanceFrequency(&Temp);
    return(Temp.QuadPart);
    
    // REFER: Debug only code, rdtsc is inaccurate if the thread it is called from is not bound 
    // to a single processor core
    // Assert(SetThreadAffinityMask(GetCurrentThread(), 1) != 0) // will only run on the first core
}

int64_t 
Win32GetTimeCounter()
{
    LARGE_INTEGER Value;
    QueryPerformanceCounter(&Value);
    return(Value.QuadPart);
}

double 
Win32GetSecondsElapsedSince(int64_t Start, int64_t Freq)
{
    LARGE_INTEGER End;
    QueryPerformanceCounter(&End);
    double Time = (double)(End.QuadPart - Start);
    Time /= (double)Freq;
    return(Time);
}


void
Win32FreeFileMemory(void* Memory)
{
    if(Memory)
    {
        VirtualFree(Memory, 0, MEM_RELEASE);
    }
}

File_Handle
Win32ReadEntireFile(const char* Filename)
{
    File_Handle Result = {0};
    HANDLE FileHandle = CreateFileA(Filename,
                                    GENERIC_READ,
                                    FILE_SHARE_READ,
                                    0,
                                    OPEN_EXISTING,
                                    0,
                                    0);
    if(FileHandle != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER FileSize;
        if(GetFileSizeEx(FileHandle, &FileSize))
        {
            uint32_t FileSize32 = SafeTruncateUInt64(FileSize.QuadPart);
            Result.Contents = VirtualAlloc(0, FileSize32, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
            if(Result.Contents)
            {
                DWORD BytesRead = 0;
                if(ReadFile(FileHandle, Result.Contents, FileSize32, &BytesRead, 0) && 
                  (BytesRead == FileSize32))
                {
                    Result.ContentSize = FileSize32;
                }
                else
                {
                    Win32FreeFileMemory(Result.Contents);
                    Result.Contents = 0;
                } 
            }
            else
            {

            }
        }
        else
        {

        }
        CloseHandle(FileHandle);        
    }
    else
    {

    }
    return(Result);
}

#include "array.h"

template<typename T>
void arrayAlloc(Array<T>& arr, memory_block* mem, int bytes)
{
    arr.Allocate((T*) PushData(mem, bytes), bytes);
}


struct ui_vars
{
    bool mIsCamFrozen;
    bool mIsBlendingEnabled;
    bool mIsAttenuationEnabled;
    real32 mBumpScaleFactor;
    real32 mDiffuseLightColor[3];
};

#include "mathlib.h"
#include "mathlib.cpp"

#include "obj_loader.h"
#include "obj_loader.cpp"

struct ui_vars;
#include "render_engine.h"
#include "render_engine.cpp"

#include "test_cube.h"
#include "input.cpp"

#include "ui.cpp"

int main(int argc, char** argv)
{
    int64 ProcFreq = Win32InitPerfFreq();
    
    uint32 screen_width = 1920;
    uint32 screen_height = 1080;
    uint32 gl_major = 3;
    uint32 gl_minor = 3;
    bool running = true;

    SDL_Window* sdl_window;
    InitSDL(&sdl_window, "Global Illumination Engine", 
            screen_width, screen_height,
            gl_major, gl_minor);
    
    if(sdl_window)
    {
        memory_block Memory;
        Memory.mTotalBytes = Gigabytes(1);
        Memory.mUsedBytes = 0;
        #ifdef _DEBUG
        LPVOID base_address = (LPVOID) Terabytes(2);
        #else
        LPVOID base_address = 0;
        #endif

        Memory.mBase = (uint8_t*) VirtualAlloc(base_address, 
                                                (size_t) Memory.mTotalBytes,
                                                MEM_RESERVE | MEM_COMMIT,
                                                PAGE_READWRITE);

        if(Memory.mBase)
        {
            render_engine* Renderer = PushStruct(render_engine, &Memory);
            {
                Renderer->mShaderCache.First = AllocateArray(Shader_Program, 10, &Memory);
                Renderer->mShaderCache.TotalCount = 10;

                Renderer->mMaterialCache.First = AllocateArray(Material, 100, &Memory);
                Renderer->mMaterialCache.TotalCount = 100;

                Renderer->mMeshes.First = AllocateArray(Mesh, 1000, &Memory);
                Renderer->mMeshes.TotalCount = 1000;

                Shader_Program default_shader;
                CreateShader(&(default_shader),
                             "../code/shaders/ptn_vshader.glsl", 
                             "../code/shaders/ptn_fshader.glsl");

                Renderer->mShaderCache.Add(default_shader);
                
                Renderer->mProjectionMatrix = PerspProjectionLH(0.1f, 5000.0f, 45.0f, 
                                (float) screen_width / (float) screen_height);
            }


            Camera eye;
            {
                eye.Target = {0.0f, 0.0f, 0.0f};
                eye.Position = {0.0f, 0.0f, -5.0f};
                eye.WorldUp = {0.0f, 1.0f, 0.0f};
                eye.Speed = 1000.0f;
                eye.Yaw = 0.0f;
                eye.Pitch = 0.0f;
                eye.CalcAxes();
                eye.CalcLookAt();
            }

            bool isInitialized = false;

            LoadOBJ("../assets/cornell-box/CornellBox-Original.obj", &Memory, Renderer);

            // TODO: concatenate(dataDirectory, filename); instead of writing ../assets/
			Cubemap skybox;
			skybox.Create("../assets/skybox/siege_rt.tga", "../assets/skybox/siege_lf.tga", 
					      "../assets/skybox/siege_up.tga", "../assets/skybox/siege_dn.tga", 
						  "../assets/skybox/siege_bk.tga", "../assets/skybox/siege_ft.tga");
            
            Light point_light;
            point_light.Position = {-1250.0f, 200.0f, 5.0f};
            point_light.Diffuse = {1.0f, 0.0f, 0.0f};
            point_light.Ambient = {0.1f, 0.1f, 0.1f};
            point_light.Specular = {1.0f, 1.0f, 1.0f};
            point_light.LinearAtt = 0.014f;
            point_light.QuadAtt = 0.000007f;

            Unit_Cube light_cube;
            Shader_Program lightshader;
            CreateShader(&lightshader, "../code/shaders/light_vshader.glsl",
                                       "../code/shaders/light_fshader.glsl");

            glViewport(0, 0, screen_width, screen_height);
            glEnable(GL_DEPTH_TEST);
            glEnable(GL_CULL_FACE);

            ui_system DearImgui;
            DearImgui.Init(screen_width, screen_height, "../code/shaders/uivshader.glsl", "../code/shaders/uifshader.glsl");
            ImGui::SetNextWindowSize(ImVec2(500, 300));
            ImGui::SetNextWindowPos(ImVec2(0, 0));

            ui_vars UiVars;
            UiVars.mIsCamFrozen = false;
            UiVars.mIsBlendingEnabled = false;
            UiVars.mIsAttenuationEnabled = false;
            UiVars.mBumpScaleFactor = 0.0f;
            point_light.Diffuse.ToFloat3(UiVars.mDiffuseLightColor);

            int64 frame_counter = Win32GetTimeCounter();

            while(running)
            {
                real64 frame_time = Win32GetSecondsElapsedSince(frame_counter, ProcFreq);
                frame_counter = Win32GetTimeCounter();

                DearImgui.NewFrame(sdl_window, frame_time);

                ///////////////////////////////////////////////////////////
                //// SECTION: input handling
                ///////////////////////////////////////////////////////////

                SDL_Event event;
                while(SDL_PollEvent(&event) != 0)
                {
                    if(event.type == SDL_KEYDOWN && 
                       event.key.keysym.sym == SDLK_ESCAPE)
                    {
                        running = false;
                    }
                    else if(event.type == SDL_KEYDOWN &&
                            event.key.keysym.sym == SDLK_f)
                    {
                        UiVars.mIsCamFrozen = UiVars.mIsCamFrozen ? false : true;
                    }

                    if(!UiVars.mIsCamFrozen)
                    {
                        HandleEvents(event, frame_time, eye);
                    }
                    DearImgui.HandleEvents(event);
                }

                ///////////////////////////////////////////////////////////
                //// ENDSECTION: input handling
                ///////////////////////////////////////////////////////////

                if(!UiVars.mIsCamFrozen)
                {
                    eye.CalcLookAt();
                }

                ///////////////////////////////////////////////////////////
                //// SECTION: Rendering
                ///////////////////////////////////////////////////////////

                {           
                    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
                    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
                    // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

                    ImGui::Checkbox("Alpha blending", &UiVars.mIsBlendingEnabled);
                    if(UiVars.mIsBlendingEnabled)
                    {
                        glEnable(GL_BLEND);
                        glBlendEquation(GL_FUNC_ADD);
                        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                    }
                    else
                    {
                        glDisable(GL_BLEND);
                    }
                }

                skybox.Render(Renderer->mProjectionMatrix, eye.LookAt);

                // TODO: Need multithreading for progress bar :-/
                // if(!isInitialized)
                // {
                //     LoadOBJ("../assets/crytek-sponza/crytek-sponza/sponza.obj",
                //             &Memory, Renderer);
                //     isInitialized = true;
                // }

                {
                    ImGui::Spacing();
                    ImGui::DragFloat("BumpScaleFactor", &UiVars.mBumpScaleFactor, 0.0f, 100.0f);

                    ImGui::Spacing();
                    ImGui::Checkbox("Attenuation", &UiVars.mIsAttenuationEnabled);

                    ImGui::DragFloat("Camera Speed", &eye.Speed, 10.0f, 1000.0f);

                    // TODO:
                    // ImGui::SliderFloat3("Light color", diffColor, 0.0f, 1.0f);
                    // point_light.Diffuse = {diffColor[0], diffColor[1], diffColor[2]};
                    // point_light.Ambient = point_light.Diffuse;
                }

                Render(Renderer, eye.LookAt, point_light, eye.Position, UiVars);

                // Render all the lights
                {
                    glUseProgram(lightshader.Program);
                    matrix model = (Translation(point_light.Position.x,
                                                   point_light.Position.y,
                                                   point_light.Position.z));
                    BindMatrix(lightshader, "model", model);
                    BindMatrix(lightshader, "view", eye.LookAt);
                    BindMatrix(lightshader, "mProjectionMatrix", Renderer->mProjectionMatrix);
                    BindVector(lightshader, "light_color", point_light.Diffuse);
                    light_cube.Render();
                }

                DearImgui.Render();
                SDL_GL_SwapWindow(sdl_window);;
                
                ///////////////////////////////////////////////////////////
                //// ENDSECTION: Rendering
                ///////////////////////////////////////////////////////////
            }

            VirtualFree(Memory.mBase, 0, MEM_RELEASE);
        }
        else
        {
            // TODO: Logging
        }
    }

    return(0);
}
