#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

enum class Severity
{
	info,
	warning,
	error
};

class Logging_System
{
	static const uint32 sLogFileSize = (1024*1024); // 1 MB
	static const uint32 sLogFilenameLen = 80;
	static const int32 sWarningLen = 11;
	static const int32 sErrorLen = 9;
	static const int32 sExpectedLen = 1024;
	static const char* sWarningStr;
	static const char* sErrorStr;

	char Filename[sLogFilenameLen];
	int32_t IsOn;
	int32_t IsHeapAllocated;
	Array<char> LogFile;

public:
	void HeapAllocate(char* OutputFilename, bool SetOn); // allocates sLogFileSize for itself
	void Log(Severity Flag, char* Fmt, ... );
	void WriteToFile();
	void HeapFree();
};

const char* Logging_System::sWarningStr = "[WARNING]: ";
const char* Logging_System::sErrorStr = "[ERROR]: ";

void
Logging_System::HeapAllocate(char* OutputFilename, bool SetOn)
{
	IsOn = SetOn;
	if(IsOn)
	{
		strncpy(Filename, OutputFilename, sLogFilenameLen);
		LogFile.Allocate((char*) malloc(sLogFileSize), sLogFileSize);
		IsHeapAllocated= true;

		time_t rawtime;
		struct tm* timeinfo;
		time(&rawtime);
		timeinfo = localtime(&rawtime);
		sprintf(LogFile.First, "Time: %s", asctime(timeinfo));
		LogFile.CurrCount = (uint32_t) strlen(LogFile.First);
	}
}

void
Logging_System::WriteToFile()
{
	if(IsOn)
	{
		FILE* fptr = fopen(Filename, "w+");
		fwrite(LogFile.First, 1, LogFile.CurrCount, fptr);
		fclose(fptr);
	}
}

void
Logging_System::HeapFree()
{
	if(IsHeapAllocated) free(LogFile.First);
}

void
Logging_System::Log(Severity Flag, char* Fmt, ... )
{
	if(IsOn)
	{
		if(LogFile.TotalCount == 0 && LogFile.First)
		{
			printf("LogFile Error: Memory not allocated for debugger.\n");
			return;
		}
		
		uint32_t unused = LogFile.TotalCount - LogFile.CurrCount;

		switch(Flag)
		{
			case Severity::info:
			{
				if(sExpectedLen > unused)
					return;
			}
			break;

			case Severity::warning:
			{
				if(sWarningLen + sExpectedLen <= unused)
				{
					strncpy(LogFile.First + LogFile.CurrCount, sWarningStr, sWarningLen);
					LogFile.CurrCount += sWarningLen;
				}
				else
				{
					printf("LogFile error: Not enough memory.\n");
					return;
				}
			}
			break;

			case Severity::error:
			{
				if(sErrorLen + sExpectedLen <= unused)
				{
					strncpy(LogFile.First + LogFile.CurrCount, sErrorStr, sErrorLen);
					LogFile.CurrCount += sErrorLen;
				}
				else
				{
					printf("LogFile error: Not enough memory.\n");
					return;
				}
			}
			break;
		}

		uint32_t prev_size = LogFile.CurrCount;

		va_list list;
		va_start(list, Fmt);
		_vsnprintf(LogFile.First + LogFile.CurrCount, unused, Fmt, list);
		va_end(list);
		
		LogFile.CurrCount = (uint32_t) strlen(LogFile.First);

		printf("%s", LogFile.First + prev_size);
	}
}