#pragma once

#include "array.h"
#include <stdint.h>
typedef uint32_t uint32;
typedef int32_t int32;
typedef uint64_t uint64;
typedef int64_t int64;
typedef uint8_t uint8;
typedef int8_t int8;
typedef float real32;
typedef double real64;

#define INTERNAL static
#define GLOBAL_VARIABLE static
#define ASSERT(Exp) if(!(Exp)) {*(int*)0 = 0;}
#define KILOBYTES(Value) ((Value)*1024LL)
#define MEGABYTES(Value) (KILOBYTES(Value)*1024LL)

struct v3;
struct Face;
struct material_spec;
struct obj_model
{
	static const int sBufferMemoryBytes = MEGABYTES(5); 
	static const int sPolyMemoryBytes = 2*sBufferMemoryBytes;
	static const int sMaterialMemoryBytes = KILOBYTES(100);

	static const int sMemReqd = 3*sBufferMemoryBytes + sPolyMemoryBytes + sMaterialMemoryBytes;

	static const int sMaxFaceVerts = 4;
	static const int sMaxFaceIndices = sMaxFaceVerts*3;
	static const int sFilenameLen = 80;

    bool IsValid;
	Array<material_spec> Materials;
	Array<v3> Positions;
	Array<v3> TexCoords;
	Array<v3> Normals;
	Array<Face> Faces;
};

obj_model ParseObjFile(int8* memory, char* filename);

struct material_spec
{
    char Name[obj_model::sFilenameLen];
    char DiffuseMap[obj_model::sFilenameLen];
    char AmbientMap[obj_model::sFilenameLen];
    char SpecMap[obj_model::sFilenameLen];
    char BumpMap[obj_model::sFilenameLen];
    char DissolveMask[obj_model::sFilenameLen];
    v3 Ka;
    v3 Kd;
    v3 Ks;
    v3 Tf;
    float Shininess;
    float Dissolve;
    uint32 Illum;
};

enum class Vertex_Type
{
    pnt,
    pn,
    pt,
    p
};

struct Face
{
    Vertex_Type Type;
    int VertexCount;
    int32 PosIndex[obj_model::sMaxFaceVerts];
    int32 TexIndex[obj_model::sMaxFaceVerts];
    int32 NormalIndex[obj_model::sMaxFaceVerts];
    int32 MatIndex;
};