For getting bump mapping working: https://en.wikipedia.org/wiki/Bump_mapping

Sensible error handling series: http://bitsquid.blogspot.ae/2012/01/sensible-error-handling-part-1.html

Getting the obj loader and the renderer working together:
1) make the obj loader callable from the renderer
2) add the debug logger to the renderer
3) handle all the different types of faces and how they should be rendered, research render groups
and read some source code on how other people have done it

FIRST, SOLVE THE DEPENDENCY MESS.