#include "platform_defs.h"
#include <sys/mman.h>
// #include <CoreServices/CoreServices.h>
// #include <mach/mach.h>
// #include <mach/mach_time.h> // for high precision timer
// read https://developer.apple.com/library/mac/qa/qa1398/_index.html

file_handle 
ReadEntireFile(const char* Filename)
{
	file_handle result = {0};

	FILE* ptr = fopen(Filename, "rw");

	// finding content size first
	int64_t start = ftell(ptr);
	fseek(ptr, 0L, SEEK_END);
	int64_t end = ftell(ptr);
	fseek(ptr, 0L, SEEK_SET);
	// TODO: make sure this truncate is safe
	result.ContentSize = (uint32_t) (end - start);

	// allocate memory for the contents
	result.Contents = mmap(0, result.ContentSize, PROT_READ | PROT_WRITE, MAP_PRIVATE, fileno(ptr), 0);
	if(result.Contents)
	{
		return(result);
	}
}