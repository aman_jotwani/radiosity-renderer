void HandleEvents(const SDL_Event& Event, real64 DeltaTime, Camera& Cam)
{
	if(Event.type == SDL_KEYDOWN)
	{
		switch(Event.key.keysym.sym)
		{
			case SDLK_w:
			{
				Cam.Position += Cam.Front*Cam.Speed*DeltaTime;
			}
			break;

			case SDLK_s:
			{
				Cam.Position -= Cam.Front*Cam.Speed*DeltaTime;
			}
			break;

			case SDLK_d:
			{
				Cam.Position += Cam.Right*Cam.Speed*DeltaTime;
			}
			break;

			case SDLK_a:
			{
				Cam.Position -= Cam.Right*Cam.Speed*DeltaTime;
			}
			break;
		}
	}
	else if(Event.type == SDL_MOUSEMOTION)
	{
		static bool first = true;
		if(first)
		{
			first = false;
		}
		else
		{
			int32_t xrel = Event.motion.xrel;
			int32_t yrel = Event.motion.yrel;

			real32 MouseSensitivity = 0.05f;

			if(xrel > 0)
				Cam.Yaw += xrel*MouseSensitivity;
			else if(xrel < 0)
				Cam.Yaw += xrel*MouseSensitivity;

			if(yrel < 0)
				Cam.Pitch -= yrel*MouseSensitivity;
			else if(yrel > 0)
				Cam.Pitch -= yrel*MouseSensitivity;

			if(Cam.Pitch >= 90.f)
				Cam.Pitch = 89.f;
			else if(Cam.Pitch <= -89.f)
				Cam.Pitch = -89.f;

			v3 Direction;
			Direction.x = cos(ToRadians(Cam.Pitch))*cos(ToRadians(Cam.Yaw));
			Direction.y = sin(ToRadians(Cam.Pitch));
			Direction.z = cos(ToRadians(Cam.Pitch))*sin(ToRadians(Cam.Yaw));
			
			Cam.Front = Direction;
			Cam.Front.Normalize();
			
			Cam.Right = CrossProduct(Cam.Front, Cam.WorldUp);
			Cam.Right.Normalize();

			Cam.Up = CrossProduct(Cam.Right, Cam.Front);
			Cam.Up.Normalize();
		}
	}
}
