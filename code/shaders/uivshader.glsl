#version 330 core

uniform mat4 Proj;

layout (location = 0) in vec2 Position;
layout (location = 1) in vec2 Uv;
layout (location = 2) in vec4 Color;

out vec2 FragUv;
out vec4 FragColor;

void main()
{
	FragUv = Uv;
	FragColor = Color;
	gl_Position = Proj * vec4(Position.xy, 0, 1);
}