#version 440 core

struct Material
{
	sampler2D AmbientMap;
	sampler2D DiffuseMap;
	sampler2D SpecMap;
	sampler2D BumpMap;
	vec3 Kd;
	vec3 Ka;
	vec3 Tf;
	uint Illum;
};

struct Light
{
	vec3 Position;
	vec3 Ambient;
	vec3 Diffuse;
};

in vec3 frag_position;
in vec3 frag_normal;
in vec2 uv;

out vec4 color;

uniform Material material;
uniform Light light;

void main()
{
	vec3 ambient = light.Ambient * material.Ka * vec3(texture(material.AmbientMap, uv));
	
	vec3 norm = normalize(frag_normal);
	vec3 lightDir = normalize(light.Position - frag_position);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = light.Diffuse * diff * material.Kd * vec3(texture(material.DiffuseMap, uv));

	color = vec4(gl_FragCoord.z, 1.0f);
}