#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 tex_coords;

out vec2 frag_uv;

uniform mat4 model;
uniform mat4 proj;
uniform mat4 view;
uniform mat4 uv_transform;

void main()
{
	gl_Position = proj * view * model * vec4(position, 1.0);
	vec4 result = uv_transform * vec4(tex_coords, 0.0, 1.0);
	frag_uv = vec2(result.x, result.y);
}