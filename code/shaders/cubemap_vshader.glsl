#version 440 core

layout (location=0) in vec3 position;

out vec3 tex_coords;

uniform mat4 projection;
uniform mat4 view;

void main()
{
	vec4 clip_coords = projection*view*vec4(position, 1.0);
	gl_Position = clip_coords;
    tex_coords = position;
}
