#version 450 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 tex_coords;
layout (location = 2) in vec3 normal;

out vec2 frag_uv;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;

void main()
{
	gl_Position = proj * view * model * vec4(position, 1.0);
	frag_uv = tex_coords.xy;
}