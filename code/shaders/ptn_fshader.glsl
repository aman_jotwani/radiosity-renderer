#version 330 core

struct Material
{
	bool HasAmbientMap;
	bool HasDiffuseMap;
	bool HasSpecMap;
	bool HasBumpMap;
	bool HasDissolveMask;
	sampler2D AmbientMap;
	sampler2D DiffuseMap;
	sampler2D SpecMap;
	sampler2D BumpMap;
	sampler2D DissolveMask;
	vec3 Kd;
	vec3 Ka;
	vec3 Ks;
	vec3 Tf;
	float Shininess;
	float Dissolve;
	uint Illum;
};

struct Point_Light
{
	vec3 Position; // this is in world space

	vec3 Ambient;
	vec3 Diffuse;
	vec3 Specular;

	float LinearAttCoeff;
	float QuadraticAttCoeff;
};

in vec3 fragPosition; // this is in world space
in vec2 fragUv;
in vec3 fragNormal;
in vec3 lightDirection;
in vec3 viewDirection;
in float fragDistFromLight;

uniform Material material;
uniform Point_Light light;
uniform float scaleFactor;
uniform bool IsAttenuationEnabled;

out vec4 color;

void main()
{
	// Blinn Phong model
	vec3 surf_normal = normalize(fragNormal);
 
 	// vec3 surf_normal = vec3(0.0, 1.0, 0.0);
	if(material.HasBumpMap)
	{
		// surf_normal = 2.0*vec3(texture(material.BumpMap, fragUv)) - vec3(1.0, 1.0, 1.0);

		// sampling from height map
		surf_normal = normalize(surf_normal);
		float xRight = textureOffset(material.BumpMap, fragUv, ivec2(1, 0)).x;
		float xLeft = textureOffset(material.BumpMap, fragUv, ivec2(-1, 0)).x;
		float yUp = textureOffset(material.BumpMap, fragUv, ivec2(0, 1)).x;
		float yDown = textureOffset(material.BumpMap, fragUv, ivec2(0, -1)).x;

		surf_normal = normalize(vec3(scaleFactor*(xLeft - xRight),
									 scaleFactor*(yDown - yUp),
									 1.0));
	}

	vec3 halfway = normalize(lightDirection + viewDirection);

	// attenuation
	float d = fragDistFromLight;
	float attenuation = 1.0 / (1.0 + light.LinearAttCoeff * d + light.QuadraticAttCoeff * d * d);
	
	// iterate over all light sources
	vec3 ambient = material.Ka;
	vec3 diffuse = material.Kd * max(dot(lightDirection, surf_normal), 0.0);
	vec3 specular = material.Ks * max(pow(dot(halfway, surf_normal), material.Shininess), 0.0);

	ambient *= vec3(texture(material.DiffuseMap, fragUv));

	vec4 temp = texture(material.DiffuseMap, fragUv);
	float alpha = 1.0;
	// alpha = temp.w;
	diffuse *= temp.xyz;

	specular *= vec3(texture(material.SpecMap, fragUv));

	if(material.HasDissolveMask)
	{
		alpha = texture(material.DissolveMask, fragUv).x;
	}

	if(!IsAttenuationEnabled)
	{
		color = vec4(ambient*light.Ambient + diffuse*light.Diffuse + specular*light.Specular, alpha);
	}
	else
	{
		color = vec4(ambient*light.Ambient + 
					 diffuse*attenuation*light.Diffuse + 
					 specular*attenuation*light.Specular, alpha);
	}
}