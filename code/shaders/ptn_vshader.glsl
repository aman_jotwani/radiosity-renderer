#version 330 core

struct Material
{
	bool HasAmbientMap;
	bool HasDiffuseMap;
	bool HasSpecMap;
	bool HasBumpMap;
	bool HasDissolveMask;
	sampler2D AmbientMap;
	sampler2D DiffuseMap;
	sampler2D SpecMap;
	sampler2D BumpMap;
	sampler2D DissolveMask;
	vec3 Kd;
	vec3 Ka;
	vec3 Ks;
	vec3 Tf;
	float Shininess;
	float Dissolve;
	uint Illum;
};

struct Point_Light
{
	vec3 Position; // this is in world space

	vec3 Ambient;
	vec3 Diffuse;
	vec3 Specular;

	float LinearAttCoeff;
	float QuadraticAttCoeff;
};

uniform mat4 proj;
uniform mat4 view;
uniform mat4 uvTransform;
uniform vec3 cameraPosition;

uniform Material material;
uniform Point_Light light;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 texCoords;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec4 tangent;

// tangent space vectors
out vec3 lightDirection; 
out vec3 viewDirection;
out vec3 fragPosition;
out vec3 fragNormal;
out vec2 fragUv;
out float fragDistFromLight;

void main()
{
	vec4 result = uvTransform * vec4(texCoords, 1.0);
	fragUv = vec2(result.x, result.y);

	fragPosition = position; 
	vec3 N = normalize(normal);
	float handedness = tangent.w;
	vec3 T = vec3(normalize(handedness * vec4(tangent.xyz, 1.0)));
	vec3 B = normalize(cross(N, T));
	mat3 TBN = transpose(mat3(T, B, N));

	fragNormal = TBN * N;
	fragDistFromLight = distance(light.Position, fragPosition);

	viewDirection = cameraPosition - fragPosition;
	lightDirection = light.Position - fragPosition;

	viewDirection = TBN * viewDirection; 
	lightDirection = TBN * lightDirection;

	viewDirection = normalize(viewDirection);
	lightDirection = normalize(lightDirection);

	gl_Position = proj * view * vec4(position, 1.0);	
}