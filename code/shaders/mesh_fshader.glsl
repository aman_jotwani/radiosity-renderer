#version 450 core

in vec2 frag_uv;

out vec4 color;

uniform sampler2D Tex;

void main()
{
	color = texture(Tex, frag_uv);
	// color = vec4(1.0, 0.0, 0.0, 1.0);
}