#version 440 core

in vec3 tex_coords;

out vec4 color;

uniform samplerCube cubemap;

void main()
{
	color = texture(cubemap, tex_coords);
	// color = vec4(vec3(LinearizeDepth(gl_FragCoord.z) / far), 1.0);
}
