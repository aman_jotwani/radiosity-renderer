#include "mathlib.h"

/////////////////////////////////////////////
// Helpers
/////////////////////////////////////////////

inline bool
IsOff(uint32_t Bitmask, uint32_t Flag)
{
	if((Bitmask & Flag) == 0)
	{
		return(true);
	}
	else
	{
		return(false);
	}
}

inline bool
IsOn(uint32_t Bitmask, uint32_t Flag)
{
	if((Bitmask & Flag) == Flag)
	{
		return(true);
	}
	else
	{
		return(false);
	}
}

inline uint32_t
TurnOff(uint32_t Bitfield, uint32_t Mask)
{
	uint32_t Result = Bitfield & (~Mask);
	return(Result);
}

///////////////////////////////////////
// Vector2
///////////////////////////////////////

v2
v2::operator-(const v2& other) const
{
	v2 result;
	result.x = x - other.x;
	result.y = y - other.y;
	return(result);
}

void
v2::operator+=(const v2& other)
{
	x += other.x;
	y += other.y;
}

v2
v2::operator*(const float Scalar) const
{
	v2 result;
	result.x = Scalar*x;
	result.y = Scalar*y;
	return(result);
}

v2
v2::operator+(const v2& other) const
{
	v2 result;
	result.x = x + other.x;
	result.y = y + other.y;
	return(result);
}

bool
v2::operator==(const v2& other) const
{
	if(x == other.x && y == other.y)
	{
		return(true);
	}
	else
	{
		return(false);
	}
}

v2
ScalarDivide(v2 V, float S)
{
	v2 result = {V.x / S, V.y / S};
	return(result);
}

////////////////////////////////////////////////
// Vector3
////////////////////////////////////////////////

v3
v3::operator-(const v3& other) const
{
	v3 result = {x - other.x, y - other.y, z - other.z};
	return(result);
}

v3
v3::operator+(const v3& other) const
{
	v3 result = {x + other.x, y + other.y, z + other.z};
	return(result);
}

v3
v3::operator*(const float& other) const
{
	v3 result = {other*x, other*y, other*z};
	return(result);
}

void 
v3::operator+=(const v3& other)
{
	x += other.x;
	y += other.y;
	z += other.z;
}

void 
v3::operator-=(const v3& other)
{
	x -= other.x;
	y -= other.y;
	z -= other.z;
}

double
v3::Length()
{
	return(sqrt(x*x + y*y + z*z));
}

void
v3::Normalize()
{
	double l = Length();
	x /= l;
	y /= l;
	z /= l;
}

v3 CrossProduct(const v3& First, const v3& Second)
{
	v3 result = {First.y*Second.z - Second.y*First.z,
				 First.z*Second.x - Second.z*First.x,
				 First.x*Second.y - Second.x*First.y};
	return(result);
}

float DotProduct(v3& First, v3& Second)
{
	float result = First.x * Second.x + First.y * Second.y + First.z * Second.z;
	return(result);
}

v3 ZeroVector()
{
	v3 result = {0.0f, 0.0f, 0.0f};
	return(result);
}

////////////////////////////////////////
// Matrix
////////////////////////////////////////

matrix::matrix()
{
	for(int i = 0; i < 16; ++i)
	{
		ColumnMajor[i] = 0.0f;
	}
}

matrix IdentityMatrix()
{
	matrix Result;
	Result.ColumnMajor[0] = 1.0f;
	Result.ColumnMajor[5] = 1.0f;
	Result.ColumnMajor[10] = 1.0f;
	Result.ColumnMajor[15] = 1.0f;
	return(Result);
}

matrix CreateFromColumns(const v3& col1, const v3& col2, const v3& col3, const v3& col4)
{
	matrix Result;
	Result.ColumnMajor[0] = col1.x;
	Result.ColumnMajor[1] = col1.y;
	Result.ColumnMajor[2] = col1.z;

	Result.ColumnMajor[4] = col2.x;
	Result.ColumnMajor[5] = col2.y;
	Result.ColumnMajor[6] = col2.z;

	Result.ColumnMajor[8] = col3.x;
	Result.ColumnMajor[9] = col3.y;
	Result.ColumnMajor[10] = col3.z;

	Result.ColumnMajor[12] = col4.x;
	Result.ColumnMajor[13] = col4.y;
	Result.ColumnMajor[14] = col4.z;

	Result.ColumnMajor[15] = 1.0f;
	
	return(Result);
}

matrix Transpose(const matrix& other)
{
	matrix result;
	int row_num = 0;
	int k = row_num;
	for(int i = 0; i < 16; i++)
	{
		result.ColumnMajor[i] = other.ColumnMajor[k];
		k += 4;
		if(k >= 16)
		{
			k = ++row_num;
		}
	}
	return(result);
}

matrix StripTranslation(matrix other)
{
	matrix result = other;
	result.ColumnMajor[12] = 0.0f;
	result.ColumnMajor[13] = 0.0f;
	result.ColumnMajor[14] = 0.0f;
	return(result);
}

matrix Translation(float x, float y, float z)
{
	matrix result;
	result.ColumnMajor[0] = 1.0f;
	result.ColumnMajor[5] = 1.0f;
	result.ColumnMajor[10] = 1.0f;
	result.ColumnMajor[12] = x;
	result.ColumnMajor[13] = y;
	result.ColumnMajor[14] = z;
	result.ColumnMajor[15] = 1.0f;
	return(result);
}

matrix Scaling(float x, float y, float z)
{
	matrix result;
	result.ColumnMajor[0] = x;
	result.ColumnMajor[5] = y;
	result.ColumnMajor[10] = z;
	result.ColumnMajor[15] = 1.0f;
	return(result);
}

matrix RotationX(float AngleInDegrees)
{
	double AngleInRadians = ToRadians(AngleInDegrees);
	matrix result;
	result.ColumnMajor[0] = 1.0f;
	result.ColumnMajor[5] = cos(AngleInRadians);
	result.ColumnMajor[6] = sin(AngleInRadians);
	result.ColumnMajor[9] = -sin(AngleInRadians);
	result.ColumnMajor[10] = cos(AngleInRadians);
	result.ColumnMajor[15] = 1.0f;
	return(result);
}

matrix RotationY(float AngleInDegrees)
{
	double AngleInRadians = ToRadians(AngleInDegrees);
	matrix result;
	result.ColumnMajor[0] = cos(AngleInRadians);
	result.ColumnMajor[2] = sin(AngleInRadians);
	result.ColumnMajor[5] = 1.0f;
	result.ColumnMajor[8] = -sin(AngleInRadians);
	result.ColumnMajor[10] = cos(AngleInRadians);
	result.ColumnMajor[15] = 1.0f;
	return(result);	
}

matrix RotationZ(float AngleInDegrees)
{
	double AngleInRadians = ToRadians(AngleInDegrees);
	matrix result;
	result.ColumnMajor[0] = cos(AngleInRadians);
	result.ColumnMajor[1] = sin(AngleInRadians);
	result.ColumnMajor[4] = -sin(AngleInRadians);
	result.ColumnMajor[5] = cos(AngleInRadians);
	result.ColumnMajor[10] = 1.0f;
	result.ColumnMajor[15] = 1.0f;
	return(result);		
}

// maps x:[0, Width], y:[0, Height] and z:[Near, Far] to OpenGL ndc unit cube
matrix OrthoProjectionOffCenterLH(float Near, float Far, float Width, float Height)
{
	matrix result;
	result.ColumnMajor[0] = 2/Width;
	result.ColumnMajor[5] = 2/Height;
	result.ColumnMajor[10] = 2/(Far-Near);
	result.ColumnMajor[12] = -1.0f;
	result.ColumnMajor[13] = -1.0f;
	result.ColumnMajor[14] = -(Far+Near)/(Far-Near);
	result.ColumnMajor[15] = 1.0f;
	return(result);
}

// maps x:[-Width/2, Width/2] y:[-Height/2, Height/2] and z:[Near, Far] to OpenGL ndc unit cube
matrix OrthoProjectionLH(float Near, float Far, float Width, float Height)
{
	matrix result;
	result.ColumnMajor[0] = 2/Width;
	result.ColumnMajor[5] = 2/Height;
	result.ColumnMajor[10] = 2/(Far-Near);
	result.ColumnMajor[14] = -(Far+Near)/(Far-Near);
	result.ColumnMajor[15] = 1.0f;
	return(result);	
}

// maps x:[-Width/2, Width/2] y:[-Height/2, Height/2] and z:[Near, Far] to OpenGL ndc unit cube
matrix OrthoProjectionRH(float Near, float Far, float Width, float Height)
{
	matrix result;
	result.ColumnMajor[0] = 2/Width;
	result.ColumnMajor[5] = 2/Height;
	result.ColumnMajor[10] = -2/(Far-Near);
	result.ColumnMajor[14] = -(Far+Near)/(Far-Near);
	result.ColumnMajor[15] = 1.0f;
	return(result);	
}

matrix PerspProjectionLH(float Near, float Far, float Fovy, float AspectRatio)
{
	matrix result;
	float tan_part = tan(ToRadians(Fovy/2));
	result.ColumnMajor[0] = 1.0f / (tan_part*AspectRatio);
	result.ColumnMajor[5] = 1.0f / tan_part;
	result.ColumnMajor[10] = (Far + Near) / (Far - Near);
	result.ColumnMajor[11] = 1.0f;
	result.ColumnMajor[14] = -2.0f*Near*Far / (Far - Near);
	return(result);
}

matrix PerspProjectionRH(float Near, float Far, float Fovy, float AspectRatio)
{
	matrix result;
	float tan_part = tan(ToRadians(Fovy/2));
	result.ColumnMajor[0] = 1.0f / (tan_part*AspectRatio);
	result.ColumnMajor[5] = 1.0f / tan_part;
	result.ColumnMajor[10] = -(Far + Near) / (Far - Near);
	result.ColumnMajor[11] = -1.0f;
	result.ColumnMajor[14] = -2.0f*Near*Far / (Far - Near);
	return(result);
}

float DotProduct(__m128 a, __m128 b)
{
	static SIMD_ALIGN float answer[4];
	const uint8_t control = 0x01;
	__m128 mulled = _mm_mul_ps(a, b);
	
	__m128 tmp0 = _mm_movehl_ps(mulled, mulled);
	__m128 tmp1 = _mm_add_ps(mulled, tmp0);
	__m128 tmp2 = _mm_shuffle_ps(tmp1, tmp1, control);
	__m128 result = _mm_add_ps(tmp1, tmp2);

	_mm_store_ps1((float*)answer, result);
	return(answer[0]);
}

matrix Mul(const matrix& first, const matrix& second)
{
	matrix result;
	__m128 frows[4];
	__m128 scols[4];

	frows[0] = _mm_load_ps((float*)first.ColumnMajor);
	frows[1] = _mm_load_ps((float*)(first.ColumnMajor + 4));
	frows[2] = _mm_load_ps((float*)(first.ColumnMajor+8));
	frows[3] = _mm_load_ps((float*)(first.ColumnMajor+12));
	_MM_TRANSPOSE4_PS(frows[0], frows[1], frows[2], frows[3]);

	scols[0] = _mm_load_ps((float*)second.ColumnMajor);
	scols[1] = _mm_load_ps((float*)(second.ColumnMajor+4));
	scols[2] = _mm_load_ps((float*)(second.ColumnMajor+8));
	scols[3] = _mm_load_ps((float*)(second.ColumnMajor+12));
	
	for(int i = 0; i < 16; ++i)
	{
		result.ColumnMajor[i] = DotProduct(frows[i/4], scols[i%4]);
	}
	result = Transpose(result);
	return(result);
}

__m128 MatVecMul(const matrix& first, const __m128 vec)
{
	__m128 result;
	__m128 frows[4];
	frows[0] = _mm_load_ps((float*)first.ColumnMajor);
	frows[1] = _mm_load_ps((float*)(first.ColumnMajor + 4));
	frows[2] = _mm_load_ps((float*)(first.ColumnMajor+8));
	frows[3] = _mm_load_ps((float*)(first.ColumnMajor+12));
	_MM_TRANSPOSE4_PS(frows[0], frows[1], frows[2], frows[3]);

	SIMD_ALIGN float vals[4];
	for(int i = 0; i < 4; ++i)
	{
		vals[i] = DotProduct(frows[i], vec);
	}
	result = _mm_load_ps((float*)vals);
	return(result);
}
