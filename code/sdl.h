#pragma once
#include <stdint.h>
#define GLEW_STATIC
#include <GL/glew.h>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_opengl.h>
#elif (__APPLE__ && __MACH__)
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#endif

#undef main // to override sdl's main

void
InitSDL(SDL_Window** Window, char* Title, 
        uint32_t Width, uint32_t Height,
        uint32_t GlMajorVersion, uint32_t GlMinorVersion);

bool IsPointInRect(SDL_Point* Point, SDL_Rect* Rect);

bool IsSameColor(SDL_Color First, SDL_Color Second);