#pragma once
#include <stdint.h>
#include <math.h>
#include <immintrin.h>

#ifdef _WIN32
#define Align(Alignment) __declspec(align((Alignment)))
#elif (__APPLE__ && __MACH__)
#define Align(Alignment) __attribute__((aligned((Alignment))))
#endif

#define SIMD_ALIGN Align(16)

static double PI = 3.1415;
#define ToRadians(x) (x)*(PI/180.0)

struct vec4
{
	float x, y, z, w;

	void operator+=(vec4 other)
	{
		x += other.x;
		y += other.y;
		z += other.z;
		w += other.w;
	};
};

struct v3
{
	float x, y, z;

	v3 operator-(const v3& other) const;
	v3 operator+(const v3& other) const;
	v3 operator*(const float& scalar) const;
	void operator+=(const v3& other);
	void operator-=(const v3& other);
	bool operator==(const v3& other) const
	{
		return((x == other.x && y == other.y && z == other.z) ? true : false);
	};
	void ToFloat3(float Arr[3])
	{
		Arr[0] = x;
		Arr[1] = y;
		Arr[2] = z;
	};
	void Normalize();
	double Length();
};

v3 ZeroVector();
float DotProduct(v3& First, v3& Second);
v3 CrossProduct(const v3& First, const v3& Second);

struct v3i
{
	uint32 x, y, z;
};

struct v2
{
	float x, y;

	v2 operator-(const v2& other) const;
	void operator+=(const v2& other);
	v2 operator*(const float Scalar) const;
	v2 operator+(const v2& other) const;
	bool operator==(const v2& other) const;	
};

struct rect
{
	// center point and dimensions
	int cx;
	int cy;
	int half_width;
	int half_height;
};

struct matrix
{
	SIMD_ALIGN float ColumnMajor[16];
	matrix();
};

matrix IdentityMatrix();
matrix CreateFromColumns(const v3& col1, const v3& col2, const v3& col3, const v3& col4);
matrix Transpose(const matrix& other);
matrix Translation(float x, float y, float z);
matrix Scaling(float x, float y, float z);
matrix RotationX(float angle);
matrix RotationY(float angle);
matrix RotationZ(float angle);

matrix Mul(const matrix& first, const matrix& second);
__m128 MatVecMul(const matrix& first, const __m128 vec);

// maps x:[0, Width], y:[0, Height] and z:[Near, Far] to OpenGL ndc unit cube
matrix OrthoProjectionOffCenterLH(float Near, float Far, float Width, float Height);

// maps x:[-Width/2, Width/2] y:[-Height/2, Height/2] and z:[Near, Far] to OpenGL ndc unit cube
matrix OrthoProjectionLH(float Near, float Far, float Width, float Height);

// maps x:[-Width/2, Width/2] y:[-Height/2, Height/2] and z:[Near, Far] to OpenGL ndc unit cube
matrix OrthoProjectionRH(float Near, float Far, float Width, float Height);

matrix PerspProjectionLH(float Near, float Far, float Fovy, float AspectRatio);